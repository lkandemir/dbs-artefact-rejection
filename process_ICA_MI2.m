function varargout = process_ICA_MI2( varargin )
% PROCESS_ICA_MI2: Mutual information. 
%
% =============================================================================@
%
% Authors: Levent Kandemir, 2018
% 
% Code inspired from:
% O. Abbasi, J. Hirschmann, G. Schmitz, A. Schnitzler, M. Butz. Rejecting 
%  deep brain stimulation artefacts from MEG data using ICA and mutual
%  information. J. Neurosci. Methods, 268 (2016), pp. 131-141.

eval(macro_method);
end


%% ===== GET DESCRIPTION =====
function sProcess = GetDescription() %#ok<DEFNU>
    % === Description the process
    sProcess.Comment     = 'DBS Artifact Removal - Mutual Information Calculation';
    sProcess.FileTag     = 'ICA-MI';
    sProcess.Category    = 'Filter';
    sProcess.SubGroup    = 'DBS_Noise';
    sProcess.Index       = 66;
    
    % === Definition of the input accepted by this process
    sProcess.InputTypes  = {'data', 'results', 'raw', 'matrix'};
    sProcess.OutputTypes = {'data', 'results', 'raw', 'matrix'};
    sProcess.nInputs     = 1;
    sProcess.nMinFiles   = 1; 
    
    % === EMG sensor attached on DBS sensor.
    sProcess.options.label2.Comment = '<B>DBS Sensors</B>';
    sProcess.options.label2.Type    = 'label';
    
    A = [".mat","Reference Signal .mat","ref"];
    C = cellstr(A);
    SelectOptions = {...
        '', ...                               % Filename
        '', ...                               % FileFormat
        'open', ...                           % Dialog type: {open,save}
        'Import reference signal...', ...     % Window title
        'ImportData', ...                     % LastUsedDir: {ImportData,ImportChannel,ImportAnat,ExportChannel,ExportData,ExportAnat,ExportProtocol,ExportImage,ExportScript}
        'single', ...                         % Selection mode: {single,multiple}
        'files', ...                          % Selection mode: {files,dirs,files_and_dirs}
        C, ... % Get all the available file formats
        'DataIn'};                          % DefaultFormats: {ChannelIn,DataIn,DipolesIn,EventsIn,MriIn,NoiseCovIn,ResultsIn,SspIn,SurfaceIn,TimefreqIn
    % Option: Event file
    sProcess.options.refsig.Comment = 'Reference Signal:';
    sProcess.options.refsig.Type    = 'filename';
    sProcess.options.refsig.Value   = SelectOptions;
    
    sProcess.options.dbssensor.Comment = 'Reference Channel:';
    sProcess.options.dbssensor.Type    = 'text';
    sProcess.options.dbssensor.Value   = '';
    sProcess.options.dbssensor.InputTypes = {'data', 'raw'};
    
    sProcess.options.label3.Comment = '<B>Rejection Mode</B>:';
    sProcess.options.label3.Type    = 'label';
    sProcess.options.auto.Comment = {'Automatic', 'Semi-Automatic', 'Artifact Removal:'; 'auto', 'semi', ''};
    sProcess.options.auto.Type    = 'radio_linelabel';
    sProcess.options.auto.Value   = 'semi';
    
    sProcess.options.label4.Comment = '<B>Rejection Percentage</B>';
    sProcess.options.label4.Type    = 'label';
    sProcess.options.rej.Comment = 'Rejection: ';
    sProcess.options.rej.Type    = 'value';
    sProcess.options.rej.Value   = {10,'% ',2};

end


%% ===== FORMAT COMMENT =====
function Comment = FormatComment(sProcess) % Giving information to the user.           
            Comment="Reference=";
            if ~isempty(sProcess.options.refsig.Value{1})
                Comment=strcat(Comment, "External");
            else
                Comment=strcat(Comment, sProcess.options.dbssensor.Value);
            end
            
            if strcmp(sProcess.options.auto.Value, 'auto')
                Comment=char(strcat(Comment, " Threshold=", num2str(sProcess.options.rej.Value{1}), "%"));
            else
                Comment=char(strcat(Comment, " Threshold=Visual"));
            end  
end


%% ===== RUN =====
function sInput = Run(sProcess,sInput) % Main function.
    

    %% Required Paths
    target=strcat(getfield(bst_get('ProtocolInfo'), 'STUDIES'),'\',sInput.SubjectName,...
        '\',getfield(bst_get('Study'), 'Name'),'\','process_settings.txt');
    projloc=strcat(getfield(bst_get('ProtocolInfo'), 'STUDIES'),'\',sInput.SubjectName,...
        '\',getfield(bst_get('Study'), 'Name'),'\','projectorica.mat');
    icaloc=strcat(getfield(bst_get('ProtocolInfo'), 'STUDIES'),'\',sInput.SubjectName,...
        '\',sInput.Condition,'\','ICAcomponents.mat');
    
    %% Load Primary Files
    mfile=load(file_fullpath(sInput.FileName));
    orjdir=strsplit(mfile.History{1,3}, ' ');
    orjdir=orjdir{end};
    
    %% Check fieldtrip installation
    ft_path=bst_get('FieldTripDir');
    oldDir = pwd;
    cd(ft_path);
    h = str2func('ft_defaults');
    feval(h);
    cd(oldDir);
    
    %% Load channels
    sMat = in_bst_channel(sInput.ChannelFile);
    label={sMat.Channel.Name}';

    %% Extract settings and load reference signals and ICA decomposition
    fsamp=mfile.F.prop.sfreq;
    thr=sProcess.options.rej.Value{1};
    aut=contains(sProcess.options.auto.Value,'semi'); 
    ref=cell(2,1);
    nref=0;
    if ~isempty(sProcess.options.refsig.Value{1,1})                         % First reference loaded as a .mat file
        nref=nref+1;
        ref{nref}=cell2mat(struct2cell(load(sProcess.options.refsig.Value{1,1})));
    end
    
    if ~isempty(sProcess.options.dbssensor.Value)                           % Second reference given as channel name
        nref=nref+1;
        sensind=contains(label,sProcess.options.dbssensor.Value);
        ref{nref}=sInput.A(sensind,:);
        if ~sum(sensind) 
            error('Wrong Reference Signal!');
        end
    end
    
    if nref==0                                                              % Reference check      
        error('At Least One Reference Should Be Given!');
    end
    
    if exist(icaloc,'file')                                                     % ICA decomposition check
        load(icaloc)
    else
        error('ICA Decomposition is Missing!');
    end
    


    %% Prepare fieldtrip structure
    hdr=ft_read_header(orjdir);
    
    data2=[];
    data2.hdr=hdr;
    data2.grad=data2.hdr.grad;
    data2.label=label;
    data2.time{1}=sInput.TimeVector(1:size(mat,2));
    data2.trial{1}=sInput.A(:,map>0);
    data2.sampleinfo=[1 size(mat,2)];
    data2.fsample=fsamp;
    
    cfg= [];
    cfg.length    = epoch;
    cfg.overlap   = 0;
    data = ft_redefinetrial(cfg, data2);
    
    selchan=label(logical(bp));
    
    dataraw=data;
    cfg= [];
    cfg.channel=selchan;
    data = ft_selectdata(cfg,data); 
    
    %% Calculate mutual information, decide on rejected components and project back
    refs=ref{1};
    refs=refs(map>0);
    
    ntr=size(data.trial,2);
    comp=comp(1:length(data.trial),1)'; %#ok<NODEF>
    
    refs=refs(1:(ntr*size(comp{1},2)));
    refs=process_ICA_MI1('divide', refs, fsamp, epoch);
    
    % Calculate mutual information
    [dat] = calcMI(comp,refs, aut,thr); 
    icacl=dat.icacl;
    
    if nref>1
        for i=2:nref
            refs=ref{i};
            refs=process_ICA_MI1('divide', refs, fsamp, epoch);
            [dat] = calcMI(comp,refs, aut, thr); 
            icacl=[icacl dat.icacl]; %#ok<AGROW>
        end
    end
    reject=unique(icacl);
    
    % Reject components
    data_cleaned= mount(data, comp, unmixing, reject, bp, fsamp, epoch, label); %#ok<NODEF>
    new=concatenatefun(data_cleaned);
    tot=new.trial{1};
    
    %% Write back to Brainstorm file
    tmpmap=find(map>0);
    nstop=ntr*size(comp{1},2);
    map2=zeros(size(map));
    map2(tmpmap(1:nstop))=1;
    sInput.A(logical(bp), logical(map2))=tot;

    misval=map&~map2;
    indmisval=find(misval);
    fprintf('\nTimes after %f should be marked as bad!',sInput.TimeVector(indmisval(1)));

    %% Projector calculation
    W=unmixing;
    Wall = zeros(length(sMat.Channel), size(W,1));
    Wall(logical(bp),:) = W';
    % Build projector structure
    proj = db_template('projector');
    proj.Components = Wall;
    proj.CompMask   = zeros(size(Wall,2), 1);   % No component selected by default
    proj.CompMask(icacl)=1;
    proj.Status     = 1;
    proj.SingVal    = 'ICA';

    %% MEGMAG: Same operations on magnetometers.
    if exist('comp1','var')
        clear Wall
        comp=comp1;
        unmixing=unmixing1;
        bp=bp1;
        
        selchan=cell(sum(bp),1);
        n=0;
        for i=1:size(bp,1)
            if bp(i)
                n=n+1;
                selchan{n}=dataraw.label{i};
            end
        end
        
        cfg= [];
        cfg.channel=selchan;
        data = ft_selectdata(cfg,dataraw); 
        
        comp=comp(1:length(data.trial),1)'; %#ok<NODEF>
        
        [dat] = calcMI(comp,refs, aut,thr); 
        icacl=dat.icacl;
    
        if nref>1
            for i=2:nref
                refs=ref{i};
                refs=divide(refs, fsamp, epoch);
                [dat] = calcMI(comp,refs, aut); 
                icacl=[icacl dat.icacl];
            end
        end
        reject=unique(icacl);

        data_cleaned= mount(data, comp, unmixing, reject, bp, fsamp, epoch, label);
        new=concatenatefun(data_cleaned);
        tot=new.trial{1};

        %% Write back to Brainstorm file
        sInput.A(logical(bp), logical(map2))=tot;
                
        %% Projector for magnetometers
        W=unmixing;
        Wall(logical(bp),:) = W';
        % Build projector structure
        proj2 = db_template('projector');
        proj2.Components = Wall;
        proj2.CompMask   = zeros(size(Wall,2), 1);   % No component selected by default
        proj2.CompMask(icacl)=1;
        proj2.Status     = 1;
        proj2.SingVal    = 'ICA';
        proj(numel(proj)+1)=proj2;
       
    end
    
    newproj=proj;
    save(projloc,'newproj');
    
    %% Save process details   
    fid = fopen(target, 'wt' );
    if aut==0
        fprintf(fid, 'Process: Automatic \n');
        fprintf(fid, 'Predefined_Percentage_Rejection: %d', thr);
    else
        fprintf(fid, 'Process: Graphical! \n');
    end
    fprintf(fid, '\n%d out of %d components rejected: %.2f %\n', length(dat.icacl), length(dat.mi), (length(dat.icacl)/length(dat.mi)*100));
    fprintf(fid, '\nReference_Location: ');
    fprintf(fid, '%s', sProcess.options.refsig.Value{1,1});
    fprintf(fid, strcat('\nReference_Signal: ', sProcess.options.dbssensor.Value));
    fclose(fid);

end


%% Mutual Information Calculation
function [data] = calcMI(comp, ref, aut, thr)

    %% Function to calculate mutual information. 
    % Use as [data] = calcMI(comp, ref, aut, thr)
    % Where:
    % --- comp (cell-array): Components saved as trials.
    % --- ref (cell-array): Reference signal saved as trials. 
    % --- aut (boolean): True if semi-automatic. 
    % --- thr(double): Percentage for rejection
    % Output 'data' contains fields below:
    % --- m_mat (cell-array): Trial based mutual information matrix. 
    % --- mi (double): Mutual information average across all channels. 
    % --- th (double): Final threshold value
    % --- icacl (double): Components to reject. 
    data=struct('m_mat',0, 'mi',0, 'th', 0, 'icacl',0);
    addpath('C:\Users\Administrator\Documents\mi');             % add the path of mi folder
   
    % Calculate mutual information for all trials and components.
    for kk=1:size(comp{1,1},1)                                  
        for j=1:length(comp)
            data.m_mat(j,kk)=mutualinfo(zscore(double(comp{j}(kk,:))),zscore(double(ref{j})))/entropy(zscore(double(comp{j}(kk,:))));    
        end
    end
    
    % Average mutual information across trials. 
    data.mi=mean(data.m_mat);
    
    % Use 95 percentile to set a threshold level. 
    thtemp=data.m_mat(:);
    data.th = prctile(thtemp,95);
    clear thtemp;

    % If semi-automatic ask for confirmation.
    if aut
        % Plot mutual information between the components and the reference.
        % Plotting Properties
        def=get(0,'defaultfigureposition');
                
        % Start Plotting
        f=figure('rend','painters','pos',[def(1) def(2) 1200 800]);
        movegui(f,'center');
        
        % Plot Mutual Info
        nICA=length(data.mi);
        x=1:nICA;
        scatter(x,data.mi);
        xlabel('#ICA Components');
        ylabel('MI with Ref');
        set(gcf,'color','w');
        title(strcat("Suggested Threshold: ", num2str(data.th), " - #Rejected Components: ", num2str(countover(data.th,data.mi))," (",num2str(floor(percent(data.th,data.mi))),"%)"));
        ff = gca;
        ff.FontSize = 10;
        axis([0 nICA 0 inf]);
        
        % Plot Threshold Bar
        h=imline (gca, [-1 nICA+1], [data.th data.th]); 
        fcn = @(pos) [pos(1,1) min(pos(2,2)); pos(2,1) min(pos(:,2))];
        setDragConstraintFcn(h,fcn);
        setColor(h,[1 0 0])  % red color
        fcn = makeConstrainToRectFcn('imline',[-1 nICA+1],[0 max(data.mi)]); % on x-axis, move between 0to 5, on y-axis, move between -2 to 10
        setPositionConstraintFcn(h,fcn);
        addNewPositionCallback(h,@(p) title(strcat("Current Threshold: ", num2str(p(3)), " - #Rejected Components: ", num2str(countover(p(3),data.mi))," (",num2str(floor(percent(p(3),data.mi))),"%)")));
        addNewPositionCallback(h,@(p) asvar(p(3)));
        
        % Place Button
        btn = uicontrol('Style', 'pushbutton', 'String', 'Save Threshold',...
        'Position', [560 10 125 30],...
        'Callback', @clfunc); 
        set(btn,'Backgroundcolor','w');
        set(btn,'FontSize',10);
        uiwait(f); disp('Threshold Set.');
    else
        lim=100-thr;
        data.th=prctile(data.mi,lim);
        fprintf('\nAutomatic rejection with %.2f percent!',thr);
    end
       
    data.icacl=find(data.mi>data.th);    
    
    function asvar(val)
            data.th = val;
    end
end
%% Project Components Back to Timeseries
function [data_cleaned] = mount(data, comp, unmixing, reject, bp, fsamp, epoch, label)

    %% Function to project components back to time space. 
    % Use as [data_cleaned] = mount(data, comp, unmixing, reject, bp, fsamp, epoch, label)
    % Where;
    % --- data (struct): Data file read with fieldtrip.
    % --- comp (struct): Independent components. 
    % --- unmixing (double): Unmixing matrix calculated by ICA.
    % --- reject (double): List of components to reject.
    % --- bp (double): Used channels. 
    % --- fsamp (double): Sampling frequency of the data.
    % --- epoch (double): Trial length in seconds.
    % Output:
    % --- data_cleaned (double): Continuous data, cleaned from artifacts.

    % Sanity Check.
    nChan=size(data.trial{1,1},1);
    if sum(bp)~=nChan
        error('Wrong Blueprint!');
    end
    
    mixing=pinv(unmixing);
    nComp=size(unmixing,1);

    nTrials=size(data.trial,2);

    labelic=cell(nComp,1);
    n=0;
    for i=1:size(label,1)
        if bp(i)
            n=n+1;
            labelic(n)=label(i);
        end
    end
    
    diff=epoch/(fsamp*epoch);
    time=cell(1,nTrials);
    for i=1:nTrials
        beg=(i-1)*epoch;
        fa=size(data.trial{i},2)/(epoch*fsamp);
        en=beg+(fa*epoch)-diff;
        res=linspace(beg,en,size(data.trial{i},2));
        time{i}=res;
    end
    
    labrun=cell(nComp,1);
    for i=1:nComp
        if i>0 && i<10
            res=strcat('runica00',int2str(i));
        elseif i>9 && i<100
            res=strcat('runica0',int2str(i));
        else
            res=strcat('runica',int2str(i));
        end
        labrun{i}=res;
    end
    
    comped=cell(1, nTrials);
    for i=1:nTrials
        comped{i}=comp{i};
    end    
    
    beta_ic.fsample=fsamp;
    beta_ic.trial=comped;
    beta_ic.topo=mixing;
    beta_ic.unmixing=unmixing;
    beta_ic.topolabel=labelic;
    beta_ic.time=time;
    beta_ic.label=labrun;
    
    cfg = [];
    cfg.component = reject; % to be removed component(s)
    data_cleaned = ft_rejectcomponent(cfg, beta_ic, data);


end


    
function concatenated=concatenatefun(data)
    % Funtion used for concatenation of trials within FieldTrip structure
    concatenated=data;
    nTrial=size(data.trial,2);
    nChan=size(data.trial{1},1);
    nTime=size(data.trial{1},2);
    tot=zeros(nChan, nTime*nTrial);
    
    for i=1:nTrial
        indb=(i-1)*nTime+1;
        inde=i*nTime;
        tot(:,indb:inde)=concatenated.trial{i};
    end
    concatenated.trial=cell(1);
    concatenated.trial{1}=tot;
    time=zeros(1, nTime*nTrial);

    temp=concatenated.time{1};
    time(1:nTime)=temp;
    for i=2:nTrial
        lt=temp(nTime);
        temp=concatenated.time{i}+lt;
        indb=(i-1)*nTime+1;
        inde=i*nTime;
        time(:,indb:inde)=temp;
        time(indb)=time(indb)+0.001;
    end

    concatenated.time=cell(1);
    concatenated.time{1}=time;
end

% Helper function for threshold selection.
function per=percent(th,dat) 
    per=sum(dat>th)/length(dat);
    per=per*100;
end

% Helper function for threshold selection. 
function n=countover(th,dat) 
    n=sum(dat>th);
end

% Helper function for threshold selection. 
function clfunc(src,event)
    closereq;
end