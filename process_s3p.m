                                                                                                                                                                                                                                                                                                                function varargout = process_s3p( varargin )
% process_s3p: S3P. 
%
% =============================================================================@
%
% Authors: Levent Kandemir, 2018
% 

eval(macro_method);
end


%% ===== GET DESCRIPTION =====
function sProcess = GetDescription() %#ok<DEFNU>
    % === Description the process
    sProcess.Comment     = 'DBS Artifact Removal - S3P SHARED';
    sProcess.FileTag     = 'S3P_SHARED';
    sProcess.Category    = 'Filter';
    sProcess.SubGroup    = 'DBS_Noise';
    sProcess.Index       = 66;
    
    % === Definition of the input accepted by this process
    sProcess.InputTypes  = {'data', 'results', 'raw', 'matrix'};
    sProcess.OutputTypes = {'data', 'results', 'raw', 'matrix'};
    sProcess.nInputs     = 1;
    sProcess.nMinFiles   = 1;
    
    % === Epoch length will be used to divide the data. 
    sProcess.options.label2.Comment = '<B>SSSP Options</B>:';
    sProcess.options.label2.Type    = 'label';
    
    sProcess.options.foi.Comment = 'Frequency Range:';
    sProcess.options.foi.Type    = 'range';
    sProcess.options.foi.Value   = {[0,150], 'Hz.', 2};
	
	sProcess.options.dim.Comment = 'Noise Subspace Dimension: ';
    sProcess.options.dim.Type    = 'combobox';
    sProcess.options.dim.Value   = {1, {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10'}};
	
    % === Epoch length will be used to divide the data. 
    sProcess.options.label3.Comment = '<B>Percentile Filter Options</B>:';
    sProcess.options.label3.Type    = 'label';
    
	sProcess.options.ftr.Comment = 'Frequencies to Reject:';
    sProcess.options.ftr.Type    = 'text';
    sProcess.options.ftr.Value   = '';
    
    sProcess.options.prc.Comment = 'Percentile Filtering:';
    sProcess.options.prc.Type    = 'value';
    sProcess.options.prc.Value   = {10,'Percent ',2};

	 % === Epoch length will be used to divide the data. 
    sProcess.options.label4.Comment = '<B>Time-Frequency Transformation Options</B>:';
    sProcess.options.label4.Type    = 'label';
    
	sProcess.options.wtype.Comment = 'Window Type:';
    sProcess.options.wtype.Type    = 'combobox';
    sProcess.options.wtype.Value   = {1, {'kaiser', 'hann'}};
    
    sProcess.options.wlen.Comment = 'Window Length:';
    sProcess.options.wlen.Type    = 'value';
    sProcess.options.wlen.Value   = {4,'Sec. ',2};
	
	sProcess.options.wstep.Comment = 'Window Overlap:';
    sProcess.options.wstep.Type    = 'value';
    sProcess.options.wstep.Value   = {2,'Sec. ',2};
    
    sProcess.options.fmax.Comment = 'Lowpass Frequency:';
    sProcess.options.fmax.Type    = 'value';
    sProcess.options.fmax.Value   = {300,'Hz. ',2};
end


%% ===== FORMAT COMMENT =====
function Comment = FormatComment(sProcess) % Giving information to the user. 
    dlist=sProcess.options.dim.Value{2};
    lim=sProcess.options.foi.Value{1};
    if strcmp(dlist{sProcess.options.dim.Value{1}}, '0')
        ns=strcat("PercentileFilter=", string(sProcess.options.prc.Value{1}));
        ns=strcat(ns, " Freqs=", sProcess.options.ftr.Value);
    else
        ns=strcat("NoiseSubspace=", dlist(sProcess.options.dim.Value{1}));
        ns=strcat(ns, " Freqs=", string(lim(1)), '-', string(lim(2)));
        if ~isempty(sProcess.options.ftr.Value)
            ns=strcat(ns, " + PercentileFilter=", string(sProcess.options.prc.Value{1}),  " Freqs=", sProcess.options.ftr.Value);
        end
    end
    Comment=char(ns);
end


%% ===== RUN =====
function sInput = Run(sProcess,sInput) % Main function.
    % Load Primary Files
    channelfile=load(file_fullpath(sInput.ChannelFile));
    temp=load(file_fullpath(sInput.FileName));
    fsamp=temp.F.prop.sfreq;

    
    %% Bad Channel & Bad Segment Rejection
    [bp, channels]=demount({channelfile.Channel(:).Type}', sInput.ChannelFlag, temp.Device);
    
    % Get Options Ready
    param=get_param(sProcess.options, fsamp, bp, channels);

    % Get map for bad segments
    map=badseg(temp.F.events, sInput.TimeVector);
    datagood=sInput.A(bp, logical(map));


    %% Required Paths
    target=strcat(getfield(bst_get('ProtocolInfo'), 'STUDIES'),'\',sInput.SubjectName,...
        '\',getfield(bst_get('Study'), 'Name'),'\','process_settings.txt');

%%
    [data_s3p, ~, ~, ~, ~]=nsa_spectralssp(datagood,fsamp,param);
    sInput.A(bp,logical(map))=data_s3p;

    %% Report
    fid = fopen(target, 'wt' );
    fprintf(fid, 'Frequency_Range: ');
    fprintf(fid, strcat(num2str(param.freqs3p(1)), '-', num2str(param.freqs3p(end))));
    fprintf(fid, '\nNS Dimension: ');
    fprintf(fid, num2str(param.s3pdim));
    fprintf(fid, '\nPf_Frequencies_to_Reject: %s', param.freqpf);
    fprintf(fid, '\nPf_Percent: %d', param.percentile);
    fprintf(fid, '\nWtype: %s', param.wind);
    fprintf(fid, '\nWlength: %d', param.winsec);
    fprintf(fid, '\nWoverlap: %d', param.winstepsec);
    fprintf(fid, '\nLpassFreq: %d', param.fmax);
    fprintf(fid, '\nRefsig: %s', param.dataref);
    fclose(fid);       

end

% Helper Functions
function freqpf=correct_freqpf(freqpf,f)
        if strcmp(freqpf,'0')
            freqpf=[];
            return; 
        end

        if ~isempty(freqpf) %% There is bug here. Maybe only one frequency specified?
            if contains(freqpf,', ')
                freqpf = strsplit(freqpf, ', ');
            elseif contains(freqpf,',')
                freqpf = strsplit(freqpf,',');
            elseif contains(freqpf, ' ')
                freqpf = strsplit(freqpf);
            elseif contains(freqpf, '-')
                freqpf = cellstr(freqpf);
            else
                tempn = str2double(freqpf);
                if ~isnan(tempn)
                    freqpf=tempn;
                else
                    error('Something Wrong with Percentile Description');
                end
            end
        else
        fprintf('Percentile filtering will not be applied!');
        freqpf=[];
        end
        clear tempn;
        
        if iscell(freqpf)
            for i=1:length(freqpf)
                rtemp=freqpf{i};
                if contains(rtemp, ',')
                    rtemp=strsplit(rtemp, ',');
                    for j=1:length(rtemp)
                        if contains(rtemp{j}, '-') 
                            tempfreqpf = strsplit(rtemp{j},'-');
                            first=str2double(tempfreqpf{1});
                            last=str2double(tempfreqpf{2});
                            ind =find(f==first):find(f==last);
                            if ~exist('tpf', 'var')
                                tpf=first;
                            end
                            tpf=[tpf f(ind)'];
                        else
                            if ~exist('tpf', 'var')
                                tpf=str2double(rtemp{j});
                            end
                            tpf=[tpf str2double(rtemp{j})];
                        end
                    end
                    
                elseif contains(rtemp, '-')            
                    tempfreqpf = strsplit(rtemp,'-');
                    first=str2double(tempfreqpf{1});
                    last=str2double(tempfreqpf{2});
                    ind =find(f==first):find(f==last);
                    if ~exist('tpf', 'var')
                        tpf=first;
                    end
                    tpf=[tpf f(ind)'];
                else
                    if ~exist('tpf', 'var')
                        tpf=str2double(rtemp);
                    else
                        tpf=[tpf str2double(rtemp)];
                    end
                end
            end
        else
            tpf=freqpf;
        end
        freqpf=unique(tpf);
end

function param=get_param(options, fsamp, bp, channels)

    chanshort=channels(bp);
    param=[];
    param.freqs3p=[0 150];
    param.fmax=300;
    param.dataref=[];
    param.percentile=50;
    param.winsec=4;
    param.winstepsec=2;
    param.freqthresh=.25;
    param.padfactor=1;
    param.goodtimeframes=[];
    param.ind_grad=find(strcmp(chanshort, 'MEG GRAD')); 
    param.ind_mag=find(strcmp(chanshort, 'MEG MAG'));
    
    if isempty(param.ind_mag) && isempty(param.ind_grad)
        param.ind_grad=find(strcmp(chanshort, 'MEG'));
    end
    
    param.freqpf=options.ftr.Value; 
    % Correction for freqpf
    winsamp=floor(param.winsec*fsamp);
    nfft=winsamp*param.padfactor;
    freq_orig = (0:(nfft*param.padfactor)/2)'.*fsamp/(nfft*param.padfactor); % frequency vector.
    [~,tempind]=min(abs(freq_orig-param.fmax));
    f=round(freq_orig(1:tempind),2);
    param.freqpf=correct_freqpf(param.freqpf,f);
    
    % Extract reference signal if any.
    param.dataref=[];
    
    % Frequency Range
    temp=options.foi.Value{1};
    if ~isempty(temp) && length(temp)==2 && temp(1)<temp(2) && temp(2)<=fsamp && temp(1)>=0
        param.freqs3p=temp;
    end
    
    % Lowpass Frequency
    temp=options.fmax.Value{1};
    if ~isempty(temp)
        param.fmax=options.fmax.Value{1};
    end
    
    % s3p Dimension    
    temp=options.dim.Value{2};
    tempind=options.dim.Value{1};
    param.s3pdim=str2double(temp{tempind});
    
    % Frequencies to Reject- Percentile Filter
    %% Correction of freqpf. Added by Levent Kandemir %BUG HERE. IF ONLY ONE RANGE IS GIVEN GIVES ERROR. SAME APPLIES TO MEG PART
   
    % Percentile Setting
    temp=options.prc.Value{1};
    if ~isempty(temp)
        param.percentile=temp;
    end
    
    % Window length for Time-Frequency
    temp=options.wlen.Value{1};
    if ~isempty(temp)
        param.winsec=temp;
    end
    
    % Overlap for Time-Frequency
    temp=options.wstep.Value{1};
    if ~isempty(temp)
        param.winstepsec=temp;
    end
    
    if param.winstepsec>param.winsec/2
    error('the winstepsec should be smaller or equal to half of winsec')
    end
    
    % Window type for Time Frequency
    temp=options.wtype.Value{2};
    tempind=options.wtype.Value{1};
    param.wind=temp{tempind};

end