DBS Artefact Rejection Algorithms used in 'Kandemir, A.L., Litvak, V., Florin, E., 2020. The comparative performance of DBS artefact rejection methods for MEG recordings, NeuroImage, 2020, https://doi.org/10.1016/j.neuroimage.2020.117057.'

MATLAB files can directly be used in Brainstorm. Please report bugs to ahmetlevent.kandemir@med.uni-duesseldorf.de. 

S3P version in the master branch works only on Elekta Neuromag data. In order to work on CTF data, please refer to s3p_ctf branch. 

Please also cite original research: 

---
Hampel Filter: Allen, D.P., 2009. A frequency domain Hampel filter for blind rejection of sinusoidal interference from electromyograms. J. Neurosci. Methods 177, 303310. https://doi.org/10.1016/j.jneumeth.2008.10.019

S3P: Ramrez, R.R., Kopell, B.H., Butson, C.R., Hiner, B.C., Baillet, S., 2011. Spectral signal space projection algorithm for frequency domain MEG and EEG denoising, whitening, and source imaging. Neuroimage 56, 7892. https://doi.org/10.1016/j.neuroimage.2011.02.002

ICA-MI: Abbasi, O., Hirschmann, J., Schmitz, G., Schnitzler, A., Butz, M., 2016. Rejecting deep brain stimulation artefacts from MEG data using ICA and mutual information. J. Neurosci. Methods 268, 131141. https://doi.org/10.1016/j.jneumeth.2016.04.010

tSSS: Taulu, S., Simola, J., 2006. Spatiotemporal signal space separation method for rejecting nearby interference in MEG measurements. Phys. Med. Biol. 51, 17591768. https://doi.org/10.1088/0031-9155/51/7/008
---

We are grateful to Rey Ramirez and Omid Abbasi for providing us with the algorithm codes of S3P (in full) and ICA-MI (in part), respectively. For tSSS algorithm codes please refer to SPM toolbox. 





