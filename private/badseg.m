function map = badseg(events, time)
    %% Function to reject bad segments. 
    % Inputs:
    % --- events (struct): structure describing events in the file
    % --- time (double-array): time vector of the recording
    % Output:
    % --- map (double-array): map of bad segments
    map=ones(1,size(time,2));
    if ~isempty(events) && any(strcmp({events.label}', 'BAD'))
        badsamp=events(strcmp({events.label}', 'BAD')).times;
        badsampcor=zeros(size(badsamp));
        for i=1:size(badsamp,2)
            [~, badsampcor(1,i)]=min(abs(time-badsamp(1,i)));
            [~, badsampcor(2,i)]=min(abs(time-badsamp(2,i)));
            map(1,badsampcor(1,i):badsampcor(2,i))=0;
        end
    end
end