function [sdata,spectral]=nsa_stft(data,srate,winsec,winstepsec,wind,fmax,padfactor)

% This function performs a time-frequency decomposition of the multivariate
% time series. It computes the short-time Fourier transform of data using 
% any windowing function or using multitapers. 
%
% usage: [sdata,spectral]=nsa_stft(data,srate,winsec,winstepsec,wind,fmax,padfactor)
%
% inputs:
%
%   data is the multivariate time series (sensors by time samples)
%
%   srate is the sampling rate or sampling frequency
%
%   winsec is the window duration in seconds for time-frequency analysis.
%       (default: winsec=4;)
%
%   winstepsec is the duration of the time step in seconds for the
%       spectrogram. There will be a time frame every winstepsec seconds.
%       (default: winstepsec=winsec/4=1);
%
%   wind can be several things: (default: wind=[2 3])
%           For single taper analysis:
%               wind can be string of a window (e.g., wind='hanning';
%               wind='kaiser'; wind='gausswin'; wind='hamming'; etc.).
%               or 
%               wind can be a numeric vector of a user-defined taper with
%               length equal to the nfft length corresponding to winsec.
%               (e.g., wind=hanning(2000));
%           For multitaper spectral analysis, wind=[NW K]; a two-element vector, where      
%               NW = time bandwidth parameter (e.g. 2, 3, or 4)
%               K = number of data tapers kept, usually 2*NW-1 (e.g. 3, 5 or 7)
%                   For multitaper option: wind=[NW K]=[2 3]; is
%                   recommended.
%               or
%               wind can be a matrix (nfft x tapers) to use user-defined
%               multitaper/multiwavelets windowing functions.
%
%   fmax is the maximum frequency of interest.
%       (default: fmax=srate/2;)
%
%   padfactor is the zero padding factor.
%       (default: padfactor=1; meaning no zero padding.)
%
% outputs:
%
%   sdata is the spectrally transformed complex time-frequency data
%           (time frames x frequencies x sensors x tapers) (for single
%           tapers the array is 3D instead of 4D)
%
%   spectral is a spectral data structure with important information about 
%   the time-frequency transfomation, including a record of the input parameters.
%   
%       spectral.freq is a vector of frequencies
%
%       spectral.time is a vector of time frames
%
%       spectral.srate is the sampling rate (srate) input, which is needed by nsa_istft.m
%
%       spectral.winsec is the winsec input, which is needed by nsa_istft.m
%
%       spectral.winstepsec is the winstepsec input, which is needed by nsa_istft.m 
%
%       spectral.wind is the wind input, which is needed by nsa_istft.m
%
%       spectral.fmax is the maximum frequency of interest input
%
%       spectral.padfactor is the padfactor input, which is needed by nsa_istft.m  
%
%       spectral.numtimes is the number of time samples in the
%           data being time-frequency transformed, which is needed by nsa_istft.m 
%
%       spectral.nfft is the length of the fft
%
%       spectral.zeropads is the number of elements to zero pad by, which is needed by nsa_istft.m  

% Copyright (C) 2011- Rey Rene Ramirez
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

if nargin<3 || isempty(winsec)
    winsec=4;
end
if nargin<4 || isempty(winstepsec)
    winstepsec=winsec/4;
end
if nargin<5 || isempty(wind)
    wind=[2 3];
end
if nargin<6 || isempty(fmax)
    fmax=srate/2;
end
if nargin<7 || isempty(padfactor)
    padfactor=1;
end
speriod=1./srate; % sampling period.
winsamp=floor(winsec*srate); % window in samples. using floor but you should use nicely rounded windows in sec (e.g., 4 sec);
winstepsamp=floor(winstepsec*srate); % overlapping window step in samples.
tim=((winsamp/2):winstepsamp:(length(data)-(winsamp/2)))*speriod; % time frames vector.
numwin=length(tim); % number of time frames.
nfft=winsamp*padfactor;
szd=size(data);
freq_orig = (0:(nfft*padfactor)/2)'.*srate/(nfft*padfactor); % frequency vector.
spectral.freq_orig=freq_orig;
spectral.fmax=fmax;
fmin=1; % tThis is a minimum frequency, set to DC.  Could be changed if interested only in higher frequencies (could be made an input), but could be dangerous.
[dum,fmax]=min(abs(freq_orig-fmax)); %#ok<ASGLU>
freq=freq_orig(fmin:fmax); % cropping frequency vector.
nfreq=length(freq); % number of frequencies.
zeropads=length(freq_orig)-nfreq; % number of Fourier coefficients ignored (and set to zero for istft.m)
if isnumeric(wind) % dealing with numeric wind inputs.
    if length(wind)==2  % a two element wind vector indicates that you want to do multitaper spectral analysis.
        NW=wind(1); % time bandwidth product.
        K=wind(2); % number of tapers.
        multitaper=1; % setting multitaper flag on.
        [E]=dpss(winsamp,NW,K); % getting slepian sequences.
        E=E*sqrt(srate); % normalizing.
        repE=permute(repmat(E,[1 1 szd(1)]),[1 3 2]);
    elseif length(wind)~=2 && size(wind,2)>1 % dealing with multitaper/multiwavelet cases in which actual numeric tapers are inputted.
        E=wind; %#ok<*NASGU> % assigning inputted taper to taper variable E.
        %multitaper=1; % setting multitaper flag on.
        multitaper=-1; % setting "wavelet" flag on.
    elseif length(wind)~=2 && size(wind,2)==1 % dealing with single taper or window inputted case.
        if size(wind,1)~=nfft
            error(['Fist dimension of window should be equal to length of FFT: ' num2str(nfft)]) % error if length of window vector is not equal to nfft.
        end
        multitaper=0; % setting multitaper flag off.
    end
elseif ischar(wind) % dealing with any matlab window function inputted as a string (e.g., wind='kaiser';  wind='hanning', etc.);
    %eval(['E=' wind '(nfft);'])
    eval(['E=' wind '(winsamp);'])    
    repE=repmat(E,[1 szd(1)]); %#ok<NODEF>
    multitaper=0; % setting multitaper flag off.
end
if multitaper==1
    sdata=zeros(numwin,nfreq,szd(1),K); % the multitaper spectral data (sdata) is a 4D array (times, frequencies, channels, tapers).
    saw=srate;
elseif multitaper==-1
    sdata=zeros(numwin,size(wind,1),szd(1));
    saw=1;
else
    sdata=zeros(numwin,nfreq,szd(1)); % single taper spectral data is a 3D array (times, frequencies, channels).
    saw=sum(abs(E)); % this is something i do to get the correct scaling of spectral densities relative to time-domain amplitudes.    
end
%%%%%%%%%%%%%%%%%%%%%%% Time-frequency transformations %%%%%%%%%%%%%%%%%%%%
for j=1:numwin  % loop through time frames.
    if (j-1)*winstepsamp+(winsamp)>szd(2)
        TSM=zeros(winsamp,szd(1)); % initialize for zero padding last data chunk data
        lastchunks=length(data(1,j*winstepsamp+1:end));
        TSM(1:lastchunks,:)=TSM(1:lastchunks,:)+data(:,j*winstepsamp+1:end)'; % insert data chunk
    else
        TSM=data(:,(j-1)*winstepsamp+(1:winsamp))'; % get time chunk.
    end
    %TSM=TSM-mean(TSM); % remove DC for each chunk (detrending) (if you do
    %this you will make DC component zero.) (don't!)
    if multitaper==1 % multi case
        xk0=fft(TSM(:,:,ones(1,K)).*repE,nfft); % multi-tapered fft.
        sdata(j,:,:,:)=xk0(fmin:fmax,:,:);
    elseif multitaper==-1
        sdata(j,:,:)=E*TSM;
    else % single case
        xk0=fft(TSM.*repE,nfft); % single-tapered fft
        sdata(j,:,:)=xk0(fmin:fmax,:);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
sdata=2*sdata/saw;
spectral.freq = freq;
spectral.time=tim;
spectral.srate=srate;
spectral.winsec=winsec;
spectral.winstepsec=winstepsec;
spectral.wind=wind;
spectral.padfactor=padfactor;
spectral.numtimes=szd(2);
spectral.nfft=nfft;
spectral.zeropads=zeropads;