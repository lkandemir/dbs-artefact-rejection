function [data]=nsa_istft(sdata,spectral)

% This function reconstructs the multivariate time series from its complex
% time-frequency coefficients using the same parameters that were used in 
% the orginal time-frequency decomposition by nsa_stft.m
%
% usage: [data]=nsa_istft(sdata,spectral)
%
% inputs:
%
%   sdata is the spectrally transformed complex time-frequency data
%       (for multitaper: time frames x frequencies x sensors x tapers) 
%       (for single window analysis: time frames x frequencies x sensors)
%
%   spectral is a spectral data structure obtained with nsa_stft.m, which 
%   contains important information about the time-frequency transfomation, 
%   including a record of the input parameters used.
%   
%       spectral.freq is a vector of frequencies
%
%       spectral.time is a vector of time frames
%
%       spectral.srate is the sampling rate (srate) inputted to nsa_stft.m
%
%       spectral.winsec is the winsec inputted to nsa_stft.m
%
%       spectral.winstepsec is the winstepsec inputted to nsa_stft.m
%
%       spectral.wind is the wind inputted to nsa_stft.m
%
%       spectral.padfactor is the padfactor inputted to nsa_stft.m
%
%       spectral.numtimes is the number of time samples in the orginal 
%           data being resynthesized.  
%
%       spectral.zeropads is the number of zeros to pad the higher 
%           frequenciesfmax
%
% outputs:
%
%   data is the reconstructed multivariate time series 
%       (sensors by time samples)

% Copyright (C) 2011- Rey Rene Ramirez
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

srate=spectral.srate;
winsec=spectral.winsec;
winstepsec=spectral.winstepsec;
wind=spectral.wind;
padfactor=spectral.padfactor;
numtimes=spectral.numtimes;
szsdata=size(sdata);
winsamp=floor(winsec*srate); % window in samples. 
winstepsamp=floor(winstepsec*srate); % overlapping window step in samples.
numwin = size(sdata, 1);
nfft=winsamp*padfactor;
if isnumeric(wind) % dealing with numeric wind inputs.
    if length(wind)==2  % a two element wind vector indicates that you want to do multitaper spectral analysis.
        NW=wind(1); % time bandwidth product.
        numtapers=wind(2); % number of tapers.
        multitaper=1; % setting multitaper flag on.
        [E]=dpss(winsamp,NW,numtapers); % getting slepian sequences.
        E=E*sqrt(srate); % normalizing.
    elseif length(wind)~=2 && size(wind,2)>1 % dealing with multitaper/multiwavelet cases in which actual numeric tapers are inputted.
        E=wind; % assigning inputted taper to taper variable E.
        multitaper=1; % setting multitaper flag on.
        numtapers=size(wind,2);
    elseif length(wind)~=2 && size(wind,2)==1 % dealing with single taper or window inputted case.
        if size(wind,1)~=nfft
            error(['Fist dimension of window should be equal to length of FFT: ' num2str(nfft)]) % error if length of window vector is not equal to nfft.
        end
        multitaper=0; % setting multitaper flag off.
        numtapers=1;
    end
elseif ischar(wind) % dealing with any matlab window function inputted as a string (e.g., wind='kaiser';  wind='hanning', etc.)
    eval(['E=' wind '(nfft);'])
    multitaper=0; % setting multitaper flag off
    numtapers=1;
end
Z=zeros(spectral.zeropads,szsdata(3));
data = zeros(szsdata(3),numtimes); % initialize data.
%%%%%%%%%%%%%%%%%%%%%%%%% Inverse transformations %%%%%%%%%%%%%%%%%%%%%%%%%
for tap=1:numtapers % loop through tapers.
    W = zeros(1,numtimes);
    if multitaper
        wind=mean(E,2)';
        %wind=(E(:,tap)'); % tried, but not as stable.
    else
        wind=E';
    end
    for j=1:numwin  % loop through time fames.
        if multitaper
            tmp = real(ifft([squeeze(sdata(j,:,:,tap)); Z]*srate/2,nfft,1,'symmetric')); % inverse fft for time frame and taper.
        else    
            tmp = real(ifft([squeeze(sdata(j,:,:)); Z]*sum(abs(wind))/2,nfft,1,'symmetric')); % inverse fft for time frame.
        end
        if (j-1)*winstepsamp+(winsamp)>numtimes % dealing with last chunks of data.
            W(j*winstepsamp+1:end) = W(j*winstepsamp+1:end) + wind(1:length(W(j*winstepsamp+1:end)));
            data(:,j*winstepsamp+1:end) = data(:,j*winstepsamp+1:end) + tmp(1:size(data(:,j*winstepsamp+1:end),2),:)';
        else
            W((j-1)*winstepsamp+(1:winsamp)) = W((j-1)*winstepsamp+(1:winsamp)) + wind;   % adding windowing functions.
            data(:,(j-1)*winstepsamp+(1:winsamp)) = data(:,(j-1)*winstepsamp+(1:winsamp)) + tmp'; % adding chunk of inverse transformed data.
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data=data./(repmat(W,[szsdata(3) 1])); % divide by added windowing functions.
data=data./numtapers; % divide by number of tapers.