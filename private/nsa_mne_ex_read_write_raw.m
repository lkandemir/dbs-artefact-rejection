function nsa_mne_ex_read_write_raw(infile,outfile,data2replace)
%
%   usage: nsa_mne_ex_read_write_raw(infile,outfile,data2replace)
%
%   This function writes a fif file outfile by replacing the data in the fif
%   file infile with the data contained in the matlab matrix data2replace.
%
%   This function is based on the MNE matlab function
%   mne_ex_read_write_raw.m written by
%   Matti Hamalainen
%   Athinoula A. Martinos Center for Biomedical Imaging
%   Massachusetts General Hospital
%   Charlestown, MA, USA

% Copyright (C) 2011- Rey Rene Ramirez
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

global FIFF;
if isempty(FIFF)
   FIFF = fiff_define_constants();
end
me = 'MNE:mne_ex_read_write_raw';
if nargin ~= 3 
    error(me,'Incorrect number of arguments');
end
%   Setup for reading the raw data
try
    raw = fiff_setup_read_raw(infile,1); 
catch
    raw = fiff_setup_read_raw(infile,0);
end
% want_meg   = true;
% want_eeg   = true;
% want_stim  = true;
% include=[];
%  try
%      picks = fiff_pick_types(raw.info,want_meg,want_eeg,want_stim,include,raw.info.bads);
%  catch
%      %Failure: Try MEG + STI101 + STI201 + STI301 - bad channels instead
%      include{1} = 'STI101';
%      include{2} = 'STI201';
%      include{3} = 'STI301';
%      try
%          picks = fiff_pick_types(raw.info,want_meg,want_eeg,want_stim,include,raw.info.bads);
%      catch
%          error(me,'%s (channel list may need modification)',mne_omit_first_line(lasterr));
%      end
%  end    
szdata2replace=size(data2replace);
picks=1:szdata2replace(1);
[outfid,cals] = fiff_start_writing_raw(outfile,raw.info,picks);
% Set up the reading parameters
from        = raw.first_samp;
to          = raw.last_samp;
quantum_sec = 10;
%quantum_sec = 0.75;
%quantum_sec = .3;
quantum     = ceil(quantum_sec*raw.info.sfreq);
%quantum     = to - from + 1;
%   Read and write all the data
first_buffer = true;
for first = from:quantum:to
    last = first+quantum-1;
    if last > to
        last = to;
    end
    try
        [ data, ~ ] = fiff_read_raw_segment(raw,first,last,picks);
    catch %#ok<*CTCH>
        fclose(raw.fid);
        fclose(outfid);
        error(me,'%s',mne_omit_first_line(lasterr)); %#ok<*LERR>
    end
    %   You can add your own miracle here
    [szdata]=size(data2replace);
    data=data2replace(:,first-from+1:last-from+1);
    %data=data2replace;
    %fprintf(1,'Writing...');
    if first_buffer
       if first > 0
           fiff_write_int(outfid,FIFF.FIFF_FIRST_SAMPLE,first);
       end
       first_buffer = false;
    end
    fiff_write_raw_buffer(outfid,data,cals);
    %fprintf(1,'[done]\n');
end
fiff_finish_writing_raw(outfid);
fclose(raw.fid);