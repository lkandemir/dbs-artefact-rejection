function [data,fiffsetup,proj,nproj,times]=nsa_read_raw_elekta(datafile,activateallprojs)

% This function reads Elekta MEG data using functions from the MNE Matlab Toolbox.
%
% usage: [data,fiffsetup,proj,nproj,times]=nsa_read_raw_elekta(datafile,activateallprojs)
%
%   input:  
%
%       datafile is the name of the raw datafile
%
%   optional input:
%
%       activateallprojs is a flag to activate all default or idle SSP
%           projections (default: activateallproj=0);
%      
%   outputs:
%
%       data is the raw time-series  
%           (Note: it includes all channels in file, including non-MEG, 
%           trigger, and Miscellaneous channels)
%
%       fiffsetup is the fiff file structure
%
%       proj is projector operator matrix in case there were projection
%           vectors. (Note: this operator is not applied to the outputted
%           data).
%
%       nproj is the number of noise projection vectors.
%       
%       times is a vector of time-points associated with the time-series.

% Copyright (C) 2008 - Rey Rene Ramirez
%
%   Authors:  Rey Rene Ramirez, Ph.D.   e-mail: rrramirez at mcw.edu
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

if nargin<2
    activateallprojs=0;
end
try
    [fiffsetup] = fiff_setup_read_raw(datafile,0);
catch %#ok<CTCH>
    [fiffsetup] = fiff_setup_read_raw(datafile,1);
    display('Detected MaxShield Data not processed by SSS.')  
end    
if activateallprojs
    display('Activating all SSP projections. But not applying them. Outputted SSP projector should be applied to outputted data.')
    [fiffsetup.info.projs.active]=deal(1);
end
[data,times] = fiff_read_raw_segment(fiffsetup);  
[proj,nproj] = mne_make_projector_info(fiffsetup.info);