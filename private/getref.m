function dataref=getref(options, label)
    %% Function to return reference signal. 
    % Inputs:
    % --- options: Brainstorm options
    % --- label: channels 
    % Output:
    % --- dataref: reference data or sensor index 
    dataref=[];
    refsig=options.refsig.Value{1,1};
    chanlabel=options.dbssensor.Value;
    if ~isempty(refsig) 
        if ~isempty(chanlabel)
        fprintf("Only one reference can be used, .mat file has priority...");
        end
        dataref=load(refsig); 
        dataref=dataref.new;
    elseif ~isempty(chanlabel)
        dataref=find(strcmp(label, chanlabel));
    end
    
end