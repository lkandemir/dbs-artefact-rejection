function [data_s3p,S3P,spectral,proj,SSP]=nsa_spectralssp(data,srate,options)

% This function performs all variants of the S^3P denoising algorithm. 
% It computes the S^3P projectors from the data, S^3P transforms it, and 
% outputs it in data_s3p. The S^3P projectors in the 3D array S3P or the 
% frequency-specific projection vectors in the structure proj can be used 
% to S3P transform any other data set using the function nsa_s3p_transform.m. 
% For example, nsa_s3p.m can be run on a room recording containing artifacts 
% but no subject, and then applied to a brain recording using nsa_s3p_transform.m.
% The behavior of nsa_s3p.m is controlled by the structure options, which 
% can be used to perform all S^3P variants, including a reference noise 
% channel algorithm, and all versions can be performed using multitaper 
% spectral analysis in addition to single taper analysis.
%
% usage: [data_s3p,S3P,spectral,proj,SSP]=nsa_spectralssp(data,srate,options)
%
% inputs:
%
%   data is the multivariate time series matrix (sensors by time-points)
%   with both gradiometers and magnetometers as in the Elekta vector view
%   system.
%
%   srate is the sampling frequency or sampling rate
%
% optional inputs:
%
%   options is a structure with many important parameters that control the
%       behavior of this program. If not included or empty, default 
%       parameters are used.
%
%   The fields of the options structure are:
%
%   options.freqs3p is a two element vector that defines the frequency
%       range to perform S^3P using a baseline noise subspace dimension.
%       (default: options.freqs3p=[0 125];)
%
%   options.s3pdim is the S^3P baseline noise dimension for each frequency, 
%       which can be zero, if one desires only to remove outlier spectral 
%       peaks via frequency specific spatial transformations. 
%       (default: options.s3pdim=4) 
%
%   options.freqpf is a vector of selective frequencies where to apply the 
%       pf-S^3P algorithm. 
%       (default: options.freqpf=[]; % don't do the pf-s3p process)
%
%       Other important functional options and examples:
%           options.freqpf='same'; % do pf-S^3P at the same frequencies as 
%               S^3P, using a sliding window (i.e., sliding window across 
%               frequencies in the range defined by options_def.freqs3p)
%               Keep in mind that this approach can remove brain spectral 
%               peaks if pf-S^3P is performed on brain data with peaks in 
%               the frequency range where pf-S^3P is applied to.
%
%           options.freqpf=[1:3]*60; % suppress USA power noise line frequencies and harmonics up to 180 Hz.
%           options.freqpf=[1:4]*50; % suppress Europe power noise line frequencies and harmonics up to 200 Hz.
%
%   options.freqthresh is a threshold to allow matches between provided noise
%       frequencies (in vector freqpf) and those obtained from the FFT.
%       This was added to deal with users that sampled at strange sampling
%       rates. (e.g., if freqpf=60; but the closest frequencies were
%       within freqpf +/- freqthresh, these neighbor frequencies will be
%       included in the actual freqpf vector to be used).
%       (default: freqthresh=.25)
%
%   options.fmax is the maximum frequency of interest in Hz, which
%       specifies the corner frequency for optional direct IFFT-based 
%       "low-pass filtering" (setting Fourier coefficients to zero before 
%       inverse Fourier transform).
%       (default: options.fmax=options.freqs3p(2)=125;).
%
%   options.dataref is a time series of channel(s) to be used as a noise reference.
%       This performs yet another extension of S^3P in which the
%       noise subspace is constructed using weights that depend on
%       reference signals. To not use this, leave empty.
%       (default: options.refind=[];)
%
%   options.winsec is the window duration in seconds for time-frequency analysis.
%       (default: options.winsec=4;)
%
%   options.winstepsec is the duration of the time step in seconds for the
%       spectrogram. There will be a time frame every options.winstepsec seconds.
%       (default: options.winstepsec=options.winsec/4;)
%
%   options.wind can be several things: (default: options.wind=[2 3])
%           For single taper analysis:
%               wind can be string of a window (e.g., wind='kaiser';
%                wind='hanning'; wind='gausswin'; wind='hamming'; etc.).
%               or 
%               wind can be a numeric vector of a user-defined taper with
%               length equal to the nfft length corresponding to winsec.
%               (e.g., wind=kaiser(2000));
%           For multitaper spectral analysis, wind=[NW K]; a two-element vector, where      
%               NW = time bandwidth parameter (e.g. NW=2;)
%               K = number of data tapers kept, usually 2*NW-1 (e.g. 3, 5 or 7)
%                   For multitaper option: wind=[NW K]=[2 3]; is
%                   recommended.
%               or
%               wind can be a matrix (nfft x tapers) to use user-defined
%               multitaper/multiwavelets windowing functions.
%
%   options.padfactor is the zero padding factor.
%       (default: options.padfactor=1; meaning no zero padding.)
%
%   options.goodtimeframes is a vector of time frames to include in the creation
%       of the S^3P projectors. This should be used only if one needs to 
%       specify selective time frames (e.g., to match a different analysis)
%       or if you want to override the outlier detection process built into the
%       function and instead use all time-frames regardless of outlier time-frames
%       with very large or very low power.
%       Note: if this input is not included or if it's empty, the function
%       will actually automatically detect outliers as time frames with
%       total power larger or smaller that the median power +/- 1.5*iqr.
%       This is done to avoid making projectors from extremely noisy
%       non-stationary time frames or time frames with odd low power (e.g.,
%       time frames in which sensors went flat).
%       (default: options.goodtimeframes=[];)
%
%   options.ind_grad is a vector of indices of good gradiometers.
%       (default: options.ind_grad=1:306; options.ind_grad(3:3:306)=[]; meaning all gradiometers)
%
%   options.ind_mag is a vector of indices of good magnetometers.
%       (default: options.ind_mag=3:3:306; meaning all magnetometers)
%
%   options.percentile is the percentile to use for the percentile filtering process
%       used to remove outlier peaks with pf-S^3P.
%       (default: options.percentile=50; % that is the median)
%
%   For example, options = 
%        winsec: 4
%    winstepsec: 1
%        freqpf: [60 120]
%        s3pdim: 1
%        refind: 'EEG061'        
%        winsec: 4
%       freqs3p: [0 125]
%   will run S3P with a baseline noise subpace dimension of 1, using the
%   EKG channel named 'EEG061' as a reference for denoising within the
%   frequency band from 0 to 125Hz (the default), and apply the percentile 
%   filtered S3P additional process only at the American power line noise 
%   frequency and first harmonic. Other unspecified parameters will take 
%   default values.
%
% outputs: 
%
%   data_s3p is the data time series after S3P projections and inverse
%       time-frequency trasnformations have been applied.
%
%   S3P is a 3D tensor containing all the S3P projection operators for all
%       frequencies. These operators can be applied to a different
%       time-frequency data (e.g., they can be computed from an empty room
%       data and applied to a brain recording data set).
%
%   spectral is a spectral data structure with important information about 
%       the time-frequency transfomation, S3P, including a record of the 
%       input parameters. This stucture is needed by nsa_s3p_transform.m      
%
%   proj is a structure with all the frequency specific projection vectors,
%       for both gradiometers and magnetometers, corresponding to the 
%       frequencies and noise dimensions, in the spectral structure. 
%       This stucture is needed by nsa_s3p_transform.m 
%
%   SSP is a structure with important information about the original rank
%       of the magnetometer and gradiometers time series, and the rank of
%       these two time series after the S^3P transformations. If the rank is
%       not reduced by S^3P, then essentially, there is no effective
%       time-domain equivalent SSP operator. However, if the S^3P
%       transformations reduced the rank of the two time series, then the
%       program automatically determines what would be the equivalent
%       projection vectors for magnetometers and gradiometers, and the
%       effective time-domain SSP projector. Note that S3P 'magically' can
%       remove noise patterns (e.g., EKG) and yet not reduced the rank of the
%       multivariate time series. Also, because it transforms the
%       gradiometers and magnetometers separately, it might appear to
%       actually increase the rank of the joint time series (if this one 
%       was processed with SSS). Regular SSP does this too! This is
%       related to how SSS combines magnetometers and gradiometers using
%       an arbitray scaling factor, before computing the multipolar moments.
%       However, time domain SSP indeed reduces the rank of the separate 
%       gradiometer and magnetometer time series. S3P does not necessarily
%       reduce the rank at all, unless the noise spatial patterns are fixed
%       across frequencies, in which case, the frequency domain SSP (FD-SSP) 
%       algorithm would have been sufficient for denoising.
%       This behavior of S3P might appear miraculous for the unitiated, 
%       but it makes perfect sense and is exactly indeed what is expected, 
%       and has very positive implications about the possible lack
%       of a need to project the leadfield matrix before source imaging in
%       the time-domain for most circumstances. If the 
%       SSP.effective_projector is not empty, then it should be applied to 
%       the lead field matrix for source imaging. 
%       For questions and/or comments, e-mail me, as these details where just 
%       briefly alluded to in the paper. -Rey
%
% Reference:
%
% Spectral signal space projection algorithm for frequency domain MEG and
% EEG denoising, whitening, and source imaging.
% Ram�rez RR, Kopell BH, Butson CR, Hiner BC, Baillet S.
% Neuroimage. 2011 May 1;56(1):78-92. Epub 2011 Feb 19.  
%
% e-mail: rrramir@uw.edu

% Copyright (C) 2011- Rey Rene Ramirez
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

if nargin<2
    error('Minimum number of input arguments is data and sampling rate')
end
%%%%%%%%%%%%%% Define Defaults Options for easy editing %%%%%%%%%%%%%%%%%%%
options_def.freqs3p=[0 125]; % This specifies the frequency band at which to obtain S3P operators that apply baseline noise subspace dimension of s3pdim.
options_def.s3pdim=1;
options_def.freqpf=(1:2)*60;
options_def.freqthresh=.25;
options_def.fmax=options_def.freqs3p(2);
options_def.dataref=[];
options_def.winsec=4;
options_def.winstepsec=options_def.winsec/4;
options_def.wind=[2 3];
options_def.padfactor=1;
options_def.goodtimeframes=[];
options_def.ind_grad=1:306; options_def.ind_grad(3:3:306)=[];
options_def.ind_mag=3:3:306;
options_def.percentile=50;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin<3 || isempty(options)
    options=options_def;
else
    % Now adding missing fields if options structure was inputted.
    if ~isfield(options,'freqs3p')
        options.freqs3p= options_def.freqs3p; 
    end
    if ~isfield(options,'s3pdim')
        options.s3pdim= options_def.fs3pdim;
    end
    if ~isfield(options,'freqpf')
        options.freqpf= options_def.freqpf;
    end
    if ~isfield(options,'freqthresh')
        options.freqthresh= options_def.freqthresh;
    end
    if ~isfield(options,'fmax')
        options.fmax=options_def.fmax;
    end
    if ~isfield(options,'dataref')
        options.dataref=options_def.dataref;
    end
    if ~isfield(options,'winsec')
        options.winsec=options_def.winsec;
    end
    if ~isfield(options,'winstepsec')
        options.winstepsec=options_def.winstepsec;
    end
    if ~isfield(options,'wind')
        options.wind=options_def.wind;
    end
    if ~isfield(options,'padfactor')
        options.padfactor=options_def.padfactor;
    end
    if ~isfield(options,'goodtimeframes')
        options.goodtimeframes=options_def.goodtimeframes;
    end
    if ~isfield(options,'ind_grad')
        options.ind_grad=options_def.ind_grad;
    end
    if ~isfield(options,'ind_mag')
        options.ind_mag=options_def.ind_mag;
    end
    if ~isfield(options,'percentile')
        options.percentile=options_def.percentile;
    end
end
freqs3p=options.freqs3p;
s3pdim=options.s3pdim;
freqpf=options.freqpf;
freqthresh=options.freqthresh;
fmax=options.fmax;
winsec=options.winsec;
winstepsec=options.winstepsec;
wind=options.wind;
padfactor=options.padfactor;
goodtimeframes=options.goodtimeframes;
ind_grad=options.ind_grad;
ind_mag=options.ind_mag;
percentile=options.percentile;
if s3pdim>0
    fmax=freqs3p(2); % This is set up to low-pass starting at the last S3P frequency.
    display(['Note since s3pdim>0, the corner frequency for frequency domain ''low pass filtering'', fmax has been set equal to freqs3p(2): ' num2str(freqs3p(2))])
    % This is done to avoid bringing back noise via the inverse fft transforms from higher frequencies
    % that have not been S^3Ped.
end
%if s3pdim==0
    %display('s3p noise subspace dimension is zero, so setting freqs3p to empty set')
    %freqs3p=[];
%end
display(['s3p noise subspace dimension is ' num2str(s3pdim)])
if isempty(freqs3p) && (ischar(freqpf)&&strcmp(freqpf,'same'))
    error('freqpf cannot be set to ''same'' if freqs3p is empty')
end
if isnumeric(wind) && length(wind)==2
    multitaper=1;
else
    multitaper=0;
end
if winstepsec>winsec/2
    error('the winstepsec should be smaller or equal to half of winsec')
elseif winstepsec>winsec/4
    if multitaper && ~isempty(fmax)
        error('For multitaper, if doing ''low-pass filtering'' by zeroing coefficients, winstepsec should be at least winsec/4, otherwise there can be some instabilities (e.g., resulting in spikes) in the inverse multitaper transform. This is just an empirical observation.')
    end
end
szdata=size(data);
%%%%%%%%%%%%%%%%%%%%%%% Time-Frequency Transformation %%%%%%%%%%%%%%%%%%%%%
[sdata,spectral]=nsa_stft(data,srate,winsec,winstepsec,wind,fmax,padfactor);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
szsdata=size(sdata);
f=spectral.freq;
lef=length(f);
if ~isempty(freqs3p) % OC check because of previous version!
    ifrhp=find(f<freqs3p(1)); %#ok<MXFND>
    ifrlp=find(f>freqs3p(2)); %#ok<MXFND>
    fmaxsspi=min(ifrlp)-1;
    fminsspi=max(ifrhp)+1;
    if isempty(fmaxsspi)
        fmaxsspi=length(f);
    end
    if isempty(fminsspi)
        fminsspi=1;
    end
    indf=fminsspi:fmaxsspi;
    freqs3p=f(indf);
end
% Getting indices for lp frequencies.
if ~isempty(fmax)
    [~,indminlp]=min(abs(f-fmax(1)));
    indlp=indminlp+1:length(f); %#ok<NASGU> % Adding a 1 to the index of indminlp to start stopping really one frequency step higher.
    indpass=1:indminlp;
else
    indpass=1:length(f);
end
if multitaper
    tfmag=squeeze(sum(sum(abs(sdata(:,:,ind_grad,:)).^2,3),4)); % sum power across tapers and magnetometers.
    tfgrad=squeeze(sum(sum(abs(sdata(:,:,ind_mag,:)).^2,3),4)); % sum power across tapers and gradiometers.
else
    tfmag=squeeze(sum(abs(sdata(:,:,ind_grad)).^2,3)); % sum power across magnetometers.
    tfgrad=squeeze(sum(abs(sdata(:,:,ind_mag)).^2,3)); % sum power across gradiometers.
end
if isempty(goodtimeframes)
    %%%%%%%%%%%%%%%%% Diagnostics of Spectral Amplitudes %%%%%%%%%%%%%%%%%%
    lpow_spectro_mag=squeeze(mean(20*log10(tfmag),2)); % log power across frequencies of magnetometers.
    lpow_spectro_grad=squeeze(mean(20*log10(tfgrad),2)); % log power across frequencies of gradiometers.
    lpow=(lpow_spectro_mag/norm(lpow_spectro_mag)) + (lpow_spectro_grad/norm(lpow_spectro_grad)); % combined log power with normalization.
    lpow=lpow-min(lpow); % the world of hacks.
    iqrfactor=1.5;
    goodtimeframes=find(lpow>prctile(lpow,25)-iqrfactor*iqr(lpow) & lpow<prctile(lpow,75)+iqrfactor*iqr(lpow) & lpow>median(lpow)/1000); % getting good time frames, and out with outlier time frames. The number 1.5 can be changed to 2 or higher to be less stringent.
    % Note: to include all time frames or any time frames of choice you can
    % input the goodtimeframes vector.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
goodtimes=zeros(floor(szdata(2)/szsdata(1)),szsdata(1));
goodtimes(:,goodtimeframes)=1;
goodtimes=reshape(goodtimes,[1 floor(szdata(2)/szsdata(1))*szsdata(1)]);
ind_goodtimes=find(goodtimes);
display(['Estimated ' num2str(length(goodtimeframes)) ' good time frames out of ' num2str(szsdata(1)) ' total time frames.' ])
if multitaper
    den=length(goodtimeframes)*szsdata(4);
else
    den=length(goodtimeframes);
end
spectrum_mag=squeeze(sum(tfmag(goodtimeframes,:),1))/den;
spectrum_grad=squeeze(sum(tfgrad(goodtimeframes,:),1))/den;
sSgr=ones(1,lef)*NaN;
sSma=ones(1,lef)*NaN;
sSgr(indpass)=spectrum_mag(indpass); % Initialize power at each frequency of interest.
sSma(indpass)=spectrum_grad(indpass);
%%%%%%%%%%%%%%%%%% Reference S^3P Algorithm %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~isempty(options.dataref)
    display('Performing Reference S3P algorithm')
    [sdataref]=nsa_stft(options.dataref,srate,winsec,winstepsec,wind,fmax,padfactor);
    sdataref=abs(sdataref);
    sdataref=mean(sdataref,3);
    if multitaper
        powsdataref=sum(sum(sum(squeeze(sdataref(goodtimeframes,:,:,:)).^2,1),2),3);
    else
        powsdataref=sum(sum(squeeze(sdataref(goodtimeframes,:,:)).^2,1),2);
    end
    sdataref=sdataref/sqrt(powsdataref);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Getting a new freqpf vector using freqthresh in case the frequency grid
% is problematic.
if ~isempty(freqpf)&&~(ischar(freqpf)&&strcmp(freqpf,'same'))
    freqpf_neighbors=[];
    for k=1:length(f)
        if any(abs(f(k)-freqpf)<freqthresh)
            freqpf_neighbors=[freqpf_neighbors f(k)]; %#ok<AGROW>
        end
    end
    freqpf=unique([freqpf(:); freqpf_neighbors(:)]);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if s3pdim>=0 % S3P with nonzero baseline noise subspace dimension.
    if ischar(freqpf)&&strcmp(freqpf,'same') % Case: pf-S3P at all frequencies with moving window across frequencies.
        freqpf=freqs3p; % No need to use freqthresh.
    end
    fs3p=unique([freqs3p; freqpf]);
elseif s3pdim==0 && ~(ischar(freqpf)&&strcmp(freqpf,'same')) % Only pf-S3P case (no baseline noise subspace dimension).
    fs3p=freqpf;
end
if isempty(fs3p)
     error('fs3p is empty, so nothing to do. maybe some inputted parameters are incompatible with each other.')
end
lefs3p=length(fs3p);
indfs3p=[];
% Making sure I get the right indices of the final fs3p frequencies, and
% taking into account possible pruning by fmax.
for k=1:lefs3p
    in=find(f==fs3p(k));
    indfs3p=[indfs3p in]; %#ok<AGROW>
end
fs3p=f(indfs3p);
spectral.fs3p=fs3p;
spectral.indfs3p=indfs3p;
lefs3p=length(fs3p);
% Intializing arrays.
S3Pdim=zeros(1,lefs3p);
S3P=complex(zeros(szdata(1),szdata(1),lefs3p));
UGR=complex(zeros(length(ind_grad),length(ind_grad),lefs3p));
UMA=complex(zeros(length(ind_mag),length(ind_mag),lefs3p));
SGR=zeros(length(ind_grad),lefs3p);
SMA=zeros(length(ind_mag),lefs3p);
for fr=1:lefs3p % Loop through S^3P frequencies to get CSD and SVD matrices
    if rem(f(indfs3p(fr)),20)==0 || fr==1
        display(['processing S3P operator for frequency ' num2str(f(indfs3p(fr)))])
    end
    % Computing cross spectral density matrix (CSDM)
    if multitaper
        G=complex(zeros(szdata(1),szdata(1)));
        for tap=1:szsdata(4)
            sdataf=(squeeze(sdata(goodtimeframes,indfs3p(fr),:,tap)))';    
            if exist('sdataref','var') % Reference S^3P algorithm option
                sdatareff=(squeeze(sdataref(goodtimeframes,indfs3p(fr),:,tap)))';
                sdataf=sdataf.*repmat(sdatareff,[szsdata(3) 1]);
            end
            G=G+(sdataf*sdataf');
        end
    else
        sdataf=(squeeze(sdata(goodtimeframes,indfs3p(fr),:)))';
        if exist('sdataref','var') % Reference S^3P algorithm option
            sdatareff=(squeeze(sdataref(goodtimeframes,indfs3p(fr),:)))';
            sdataf=sdataf.*repmat(sdatareff,[szsdata(3) 1]);
        end
        G=sdataf*sdataf';
    end
    G=G./den; % Cross spectral density matrix (CSDM)
    [Ugr,Sgr]=svd(G(ind_grad,ind_grad)); % SVD of gradiometer CSDM.
    [Uma,Sma]=svd(G(ind_mag,ind_mag)); % SVD of magnetometer CSDM.
    Sgr=diag(Sgr); % Gradiometer singular values.
    Sma=diag(Sma); % Magnetometer singular values.
    UGR(:,:,fr)=Ugr; % Gradiometer singular vectors.
    UMA(:,:,fr)=Uma; % Magnetometer singular vectors.
    SGR(:,fr)=Sgr;
    SMA(:,fr)=Sma; 
    sSgr(indfs3p(fr))=sum(Sgr(s3pdim+1:end)); % Gradiometer power after baseline noise subspace dimension has been removed.
    sSma(indfs3p(fr))=sum(Sma(s3pdim+1:end)); % Magnetometer power after baseline noise subspace dimension has been removed.
end
%%%%% Percentile filtering %%%%%%
mp_gr=sSgr;
mp_ma=sSma;
for k=1:length(freqpf) 
    [~,indff]=min(abs(freqpf(k)-f));
    % Here some sliding window across frequencies is assumed. It could be
    % modified, or be included as a new input argument in options, which could be a
    % vector to make frequency window size a function of frequency (as in
    % previous incarnation of program).
    ff=4; % That means 4 frequency elements (not in Hz).
    mp_gr(indff)=prctile(sSgr(max([1 indff-ff]):min([indff+ff lef])),percentile); 
    mp_ma(indff)=prctile(sSma(max([1 indff-ff]):min([indff+ff lef])),percentile);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
proj=[];
for fr=1:lefs3p  % Loop through S^3P frequencies to get S^3P noise dimensions
    Ugr=UGR(:,:,fr);
    Uma=UMA(:,:,fr);
    Sgr=SGR(:,fr);
    Sma=SMA(:,fr);
    kk=0;
    s3pdim_gr=s3pdim;
    % While loop gets the right gradiometer noise subspace dimension
    while sSgr(indfs3p(fr))>mp_gr(indfs3p(fr))
        kk=kk+1; 
        sSgr(indfs3p(fr))=sum(Sgr(s3pdim+kk+1:end));
        s3pdim_gr=s3pdim+kk;
    end
    kk=0;
    s3pdim_ma=s3pdim;
    % While loop gets the right magnetometer noise subspace dimension
    while sSma(indfs3p(fr))>mp_ma(indfs3p(fr))
        kk=kk+1;       
        sSma(indfs3p(fr))=sum(Sma(s3pdim+kk+1:end));
        s3pdim_ma=s3pdim+kk;
    end
    Uall=zeros(szdata(1),s3pdim_gr+s3pdim_ma);
    Uall(ind_grad,1:s3pdim_gr)=Ugr(:,1:s3pdim_gr);
    Uall(ind_mag,s3pdim_gr+1:end)=Uma(:,1:s3pdim_ma);
    [Uall,Sall]=svd(Uall,'econ');
    Sall=diag(Sall);
    s3pdim_all=s3pdim_gr+s3pdim_ma;
    for kk = 1:s3pdim_all  % Loop equivalent to mne_make_projector.m
        if Sall(kk)/Sall(1) < 1e-2
            s3pdim_all = kk;
            break;
        end
    end
    Uall=Uall(:,1:s3pdim_all);
    proj.grad{fr}=Ugr(:,1:s3pdim_gr);
    proj.mag{fr}=Uma(:,1:s3pdim_ma);
    sspf=eye(szdata(1))-Uall(:,1:s3pdim_all)*Uall(:,1:s3pdim_all)'; % Construct frequency-specific S^3P projector
    S3P(:,:,fr)=sspf; % Insert in S3P 3D array
    S3Pdim(fr)=s3pdim_all; 
    % Perform frequency-specific spatial projection 
    if multitaper
        for tap=1:szsdata(4)
            sdata(:,indfs3p(fr),:,tap)=(sspf*(squeeze(sdata(:,indfs3p(fr),:,tap))'))';
        end
    else       
        sdata(:,indfs3p(fr),:)=(sspf*squeeze(sdata(:,indfs3p(fr),:))')';
    end
end
%%%%%%%%%%%%%%%%% Inverse Time-Frequency Transformation %%%%%%%%%%%%%%%%%%%
[data_s3p]=nsa_istft(sdata,spectral);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now doing some additional computations of the rank of the original data
% and of the data after S^3P to determine how S^3P has affected the rank
% and whether there is an effective time-domain SSP operator.
display('Now checking ranks and getting effective SSP information')
G=data(:,ind_goodtimes)*data(:,ind_goodtimes)';
SSP.original_rank_grad=rank((G(ind_grad,ind_grad)));
SSP.original_rank_mag=rank((G(ind_mag,ind_mag)));
display(['Rank of original time-domain gradiometer data is ' num2str(SSP.original_rank_grad)])
display(['Rank of original time-domain magnetometer data is ' num2str(SSP.original_rank_mag)])
Gs3p=data_s3p(:,ind_goodtimes)*data_s3p(:,ind_goodtimes)';
SSP.s3p_rank_grad=rank((Gs3p(ind_grad,ind_grad)));
SSP.s3p_rank_mag=rank((Gs3p(ind_mag,ind_mag)));
display(['Rank of s3p time-domain gradiometer data is ' num2str(SSP.s3p_rank_grad)])
display(['Rank of s3p time-domain magnetometer data is ' num2str(SSP.s3p_rank_mag)])
SSP.effective_projector=[];
SSP.effective_projvec_grad=[];
SSP.effective_projvec_mag=[];
if SSP.s3p_rank_grad<SSP.original_rank_grad
    Us3p_grad=svd(Gs3p(ind_grad,ind_grad));
    SSP.effective_projvec_grad=Us3p_grad(:,1+SSP.s3p_rank_grad:SSP.original_rank_grad);  
end
if SSP.s3p_rank_mag<SSP.original_rank_mag
    Us3p_mag=svd(Gs3p(ind_mag,ind_mag));
    SSP.effective_projvec_mag=Us3p_mag(:,1+SSP.s3p_rank_mag:SSP.original_rank_mag);
end
if SSP.s3p_rank_grad<SSP.original_rank_grad || SSP.s3p_rank_mag<SSP.original_rank_mag
    Uall=zeros(szdata(1),size(SSP.effective_projvec_grad,2)+size(SSP.effective_projvec_mag,2));
    Uall(ind_grad,1:size(SSP.effective_projvec_grad,2))=SSP.effective_projvec_grad;
    Uall(ind_mag,1:size(SSP.effective_projvec_mag,2))=SSP.effective_projvec_mag;
    [Uall,Sall]=svd(Uall,'econ');
    Sall=diag(Sall);
    sspdim_all=size(Uall,2);
    for kk = 1:sspdim_all  % Loop equivalent to mne_make_projector.m
        if Sall(kk)/Sall(1) < 1e-2
            sspdim_all = kk;
            break;
        end
    end
    Uall=Uall(:,1:sspdim_all);
    SSP.effective_projector=eye(szdata(1))-Uall*Uall';
end
spectral.S3Pdim=S3Pdim;
spectral.goodtimeframes=goodtimeframes;
spectral.ind_grad=ind_grad;
spectral.ind_mag=ind_mag;
spectral.multitaper=multitaper;