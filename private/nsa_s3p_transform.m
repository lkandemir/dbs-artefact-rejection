function [data_s3p,S3P,SSP]=nsa_s3p_transform(infile,outfile,spectral,varargin)

% This function applies the S^3P projectors to the data stored in the infile
% fif file and writes the transformed data to the outfile fif file. The 
% spectral stucture contains information about the S^3P and the
% time-frequency transformations and should correspond to the S^3P projector
% inputted (see nsa_s3p.m). The S^3P projector to be applied is inputted as 
% the first argument after spectral (i.e., varargin), or can be constructed
% on the fly from one or many proj stuctures (see nsa_s3p.m) containing all
% frequency specififc projection vectors.
%
% usage: [data_s3p,S3P,SSP]=nsa_s3p_transform(infile,outfile,spectral,varargin)
%
% inputs: 
%
%   infile is the name of the fif file to apply S3P on.
%
%   outfile is the name of the fif file to output.
%    
%   spectral is a spectral data structure with important information about 
%       the time-frequency transfomation, S3P, including a record of the 
%       input parameters. This stucture is obtained with nsa_s3p.m   
%
%   {varargin} means variable input arguments and can be two different
%   types of inputs:
%
%   1) the S3P projector 3D array obtained from nsa_s3p.m, or
%
%   2) the proj stucture(s) contaning all frequency-specific projection 
%       vectors, which was obtained with nsa_s3p.m
%
%       This second option is made available in case you have computed
%       multiple proj stuctures and want to construct a single S3P
%       projector from these multiple proj stuctures, which could be
%       modeling different aspects of noise, even from different
%       recordings.
%
% outputs: 
%
%   data_s3p is the data time series after S3P projections and inverse
%       time-frequency trasnformation.
%
%   S3P is a 3D tensor containing all the S3P projection operators for all
%       frequencies. It will be the same as the inputted S3P if this was
%       inputted directly, as opposed to inputting the proj structure(s).
%
%   SSP is a structure with important information about the original rank
%       of the magnetometer and gradiometers time series, and the rank of
%       these two time series after the S^3P transformations. If the rank is
%       not reduced by S^3P, then essentially, there is no effective
%       time-domain equivalent SSP operator. However, if the S^3P
%       transformations reduced the rank of the two time series, then the
%       program automatically determines what would be the equivalent
%       projection vectors for magnetometers and gradiometers, and the
%       effective time-domain SSP projector. Note that S3P 'magically' can
%       remove noise patterns (e.g., EKG) and yet not reduced the rank of the
%       multivariate time series. Also, because it transforms the
%       gradiometers and magnetometers separately, it might appear to
%       actually increase the rank of the joint time series (if this one 
%       was processed with SSS). Regular SSP does this too! This is
%       related to how SSS combines magnetometers and gradiometers using
%       an arbitray scaling factor, before computing the multipolar moments.
%       However, time domain SSP indeed reduces the rank of the separate 
%       gradiometer and magnetometer time series. S3P does not necessarily
%       reduce the rank at all, unless the noise spatial patterns are fixed
%       across frequencies, in which case, the frequency domain SSP (FD-SSP) 
%       algorithm would have been sufficient for denoising.
%       This behavior of S3P might appear miraculous for the unitiated, 
%       but it makes perfect sense and is exactly indeed what is expected, 
%       and has very positive implications about the possible lack
%       of a need to project the leadfield matrix before source imaging in
%       the time-domain for most circumstances. If the 
%       SSP.effective_projector is not empty, then it should be applied to 
%       the lead field matrix for source imaging. 
%       For questions and/or comments, e-mail me, as these details where just 
%       briefly alluded to in the paper. -Rey
%
% Reference:
%
% Spectral signal space projection algorithm for frequency domain MEG and
% EEG denoising, whitening, and source imaging.
% Ram�rez RR, Kopell BH, Butson CR, Hiner BC, Baillet S.
% Neuroimage. 2011 May 1;56(1):78-92. Epub 2011 Feb 19.  
%
% e-mail: rrramir@uw.edu

% Copyright (C) 2011- Rey Rene Ramirez
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

if nargin<4
    error('At least one proj structure corresponding to parameters in the spectral stucture (both obtained with nsa_s3p.m) needs to be inputted.')
end
ind_grad=spectral.ind_grad;
ind_mag=spectral.ind_mag;
ind_all=unique([ind_grad ind_mag]);
numch=length(ind_all);
srate=spectral.srate;
winsec=spectral.winsec;
winstepsec=spectral.winstepsec;
wind=spectral.wind;
fmax=spectral.fmax;
indfs3p=spectral.indfs3p;
fs3p=spectral.fs3p;
lfs3p=length(fs3p);
% Check frequency compatibility in terms of sizes.
% This assumes the spectral and proj inputted
% stuctures match up. This is left to the user.
lV=length(varargin);
for k=1:lV
    if lfs3p~=length(varargin{k}.grad)
        error('The number of frequencies in spectral structue is inconsistent with the number of projection vectors in one of the proj stuctures.')
    end
end
padfactor=spectral.padfactor;
if lV==1 && isnumeric(varargin{1}) % Case in which you input the S3P 3D array
    S3P=varargin{1};
else
    % Reforming S3P frequency-specific operators from proj stuctures
    S3P=complex(zeros(numch,numch,lfs3p));
    for fr=1:lfs3p
        dim=0;
        Uall(ind_all,1)=complex(zeros(numch,1)); %#ok<*AGROW>
        for k=1:length(varargin)
            aaa=varargin{k}.grad{fr};
            for g=1:size(aaa,2)
                dim=dim+1;
                Uall(ind_all,dim)=complex(zeros(numch,1));
                Uall(ind_grad,dim)=aaa(:,g);
            end
            bbb=varargin{k}.mag{fr};
            for m=1:size(bbb,2)
                dim=dim+1;
                Uall(ind_all,dim)=complex(zeros(numch,1));
                Uall(ind_mag,dim)=bbb(:,m);
            end
        end
        [Uall,Sall]=svd(Uall,'econ');
        Sall=diag(Sall);
        for kk = 1:dim
            if Sall(kk)/Sall(1) < 1e-2
                dim = kk;
                break;
            end
        end
        display(['Noise subspace dimension is ' num2str(dim) ' for frequency ' num2str(fs3p(fr))])
        Uall=Uall(:,1:dim);
        S3P(:,:,fr)=eye(length(ind_all))-Uall(:,1:dim)*Uall(:,1:dim)';
    end
end
% Reading data from infile
[data,fiffsetup]=nsa_read_raw_elekta(infile);
if srate~=fiffsetup.info.sfreq
    error('Sorry, for now the sampling rate of the data used to compute the S3P projectors needs to perfectly match the sampling rate of the data being transformed by the S3P projectors.') % This could be changed in the future.
end
data=data(ind_all,:);
%%%%%%%%%%%%%%%%%%%%%%% Time-Frequency Transformation %%%%%%%%%%%%%%%%%%%%%
[sdata]=nsa_stft(data,srate,winsec,winstepsec,wind,fmax,padfactor);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
szsdata=size(sdata);
% S3P frequency-specific spatial projections
for fr=1:lfs3p  
    sspf=S3P(:,:,fr);
    if spectral.multitaper
        for tap=1:szsdata(4)
            sdata(:,indfs3p(fr),:,tap)=(sspf*(squeeze(sdata(:,indfs3p(fr),:,tap))'))';
        end
    else       
        sdata(:,indfs3p(fr),:)=(sspf*squeeze(sdata(:,indfs3p(fr),:))')';
    end
end
%%%%%%%%%%%%%%%%% Inverse Time-Frequency Transformation %%%%%%%%%%%%%%%%%%%
[data_s3p]=nsa_istft(sdata,spectral);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if isfield(spectral,'DCoffset') && spectral.DCoffset 
    display('Removing mean of each channel')
    data_s3pmean=mean(data_s3p,2);
    data_s3p=data_s3p-repmat(data_s3pmean,[1 size(data_s3p,2)]);
end
% Writing s3p time-domain data to outfile
display('Now writing new fif file')
tic; nsa_mne_ex_read_write_raw(infile,outfile,data_s3p); toc;
% Now doing some additional computations of the rank of the original data
% and of the data after S^3P to determine how S^3P has affected the rank
% and whether there is an effective time-domain SSP operator.
display('Saved data to fif file, now checking ranks and getting effective SSP information')
G=data*data';
SSP.original_rank_grad=rank(G(ind_grad,ind_grad));
SSP.original_rank_mag=rank(G(ind_mag,ind_mag));
display(['Rank of original time-domain gradiometer data is ' num2str(SSP.original_rank_grad)])
display(['Rank of original time-domain magnetometer data is ' num2str(SSP.original_rank_mag)])
[data_s3p]=nsa_istft(sdata,spectral);
Gs3p=data_s3p*data_s3p';
SSP.s3p_rank_grad=rank(Gs3p(ind_grad,ind_grad));
SSP.s3p_rank_mag=rank(Gs3p(ind_mag,ind_mag));
display(['Rank of s3p time-domain gradiometer data is ' num2str(SSP.s3p_rank_grad)])
display(['Rank of s3p time-domain magnetometer data is ' num2str(SSP.s3p_rank_mag)])
SSP.effective_projector=[];
SSP.effective_projvec_grad=[];
SSP.effective_projvec_mag=[];
if SSP.s3p_rank_grad<SSP.original_rank_grad
    Us3p_grad=svd(Gs3p(ind_grad,ind_grad));
    SSP.effective_projvec_grad=Us3p_grad(:,1+SSP.s3p_rank_grad:SSP.original_rank_grad);  
end
if SSP.s3p_rank_mag<SSP.original_rank_mag
    Us3p_mag=svd(Gs3p(ind_mag,ind_mag));
    SSP.effective_projvec_mag=Us3p_mag(:,1+SSP.s3p_rank_mag:SSP.original_rank_mag);
end
if SSP.s3p_rank_grad<SSP.original_rank_grad || SSP.s3p_rank_mag<SSP.original_rank_mag
    Uall=zeros(szdata(1),size(SSP.effective_projvec_grad,2)+size(SSP.effective_projvec_mag,2));
    Uall(ind_grad,1:size(SSP.effective_projvec_grad,2))=SSP.effective_projvec_grad;
    Uall(ind_mag,1:size(SSP.effective_projvec_mag,2))=SSP.effective_projvec_mag;
    [Uall,Sall]=svd(Uall,'econ');
    Sall=diag(Sall);
    sspdim_all=size(Uall,2);
    for kk = 1:sspdim_all  % Loop equivalent to mne_make_projector.m
        if Sall(kk)/Sall(1) < 1e-2
            sspdim_all = kk;
            break;
        end
    end
    Uall=Uall(:,1:sspdim_all);
    SSP.effective_projector=eye(szdata(1))-Uall*Uall';
end