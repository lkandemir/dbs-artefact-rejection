function [data_s3p,S3P,SSSPdim,f,spec,proj,goodtimeframes,mp_gr,mp_ma]=nsa_pf_spectralssp_selfreqs_FASTEST(data,srate,freqs3p,s3pdim,freqpf,freqthresh,freqstop,winsec,winstepsec,wind,padfactor,goodtimeframes,ind_grad,ind_mag,percentile)

% usage: [data_s3p,S3P,SSSPdim,f,spec,proj,goodtimeframes,mp_gr,mp_ma]=nsa_pf_spectralssp_selfreqs(data,srate,freqpf,freqthresh,s3pdim,winsec,padfactor,goodtimeframes,ind_grad,ind_mag)
%
% inputs:
%
%   data is the multivariate time series matrix (sensors by time-points)
%
%   srate is the sampling frequency
%
%   freqpf this is a vector of selective frequencies where to apply the 
%       pf-S^3P algorithm. Note how this version of the algorithm is different
%       from the more basic pf-S^3P algorithm that applies the percentile 
%       filtered process to all frequencies.
%
%   freqthresh is a threshold to allow matches between provide noise
%       frequencies (in vector freqpf) and those obtained from the FFT.
%       This was added to deal with users that sampled at weird sampling
%       rates. (e.g., if freqpf=60; but the closest frequencies were
%       within freqpf +/- freqthresh, these neighbor frequencies will be
%       included in the actual freqpf vector to be used).
%       (default: freqthresh=.35)
%
%   s3pdim is the s3p baseline noise dimension for each frequency, 
%       which can be zero, if one desires only to remove outlier spectral 
%       peaks. (default: s3pdim=4)
%
%   winsec is the window duration in seconds for time-frequency analysis.
%       (default: winsec=4;)
%
%   winstepsec is the duration of the time step in seconds for the
%       spectrogram. (default: winstepsec=winsec/2;)
%
%   wind can be several things: (default: wind=[2 3])
%           For single taper analysis:
%               wind can be string of a window (e.g., wind='hanning';
%               wind='kaiser'; wind='gausswin'; wind='hamming'; etc.).
%               or 
%               wind can be a numeric vector of a user-defined taper with
%               length equal to the nfft length corresponding to winsec.
%               (e.g., wind=hanning(2000));
%           For multitaper spectral analysis, wind=[NW K]; a two-element vector, where      
%               NW = time bandwidth parameter (e.g. 2, 3, or 4)
%               K = number of data tapers kept, usually 2*NW-1 (e.g. 3, 5 or 7)
%                   For multitaper option: wind=[NW K]=[2 3]; is
%                   recommended.
%               or
%               wind can be a matrix (nfft x tapers) to use user-defined
%               multitapers/multiwavelets transforms.
%
%   padfactor is the zero padding factor.
%       (default: padfactor=1; meaning no zero padding.)
%
%   goodtimeframes is a vector of time frames to include in the creation
%       of the S3P projectors. This should be used only if one needs to 
%       specify selective time frames (e.g., to match a different analysis).
%       Note: if this input is not included or if it's empty, the function
%       will actually automatically detect outliers as time frames with
%       total power larger or smaller that the median power +/- 1.5*iqr.
%       This is done to avoid making projectors from extremely noisy
%       non-stationary time frames or time frames with odd low power.
%       (default: goodtimeframes=[];)
%
%   ind_grad is a vector of indices to good gradiometers.
%       (default: ind_grad=1:306; ind_grad(3:3:306)=[]; meaning all gradiometers)
%
%   ind_mag is a vector of indices to good magnetometers.
%       (default: ind_mag=3:3:306; meaning all magnetometers)
%
%   percentile is the percentile to use for the percentil filtering process
%       used to remove outlier peaks.
%       (default: percentile=50;)
%
% outputs: 
%
%   data_s3p is the data time series after S3P projections and inverse
%       time-frequency trasnformation. Note: This data should not be
%       localized like any regular time-series as spatial projections have
%       been applied to specific frequencies, and hence it has to be
%       localized in the frequency domain. It is important you understand
%       the meaning of this statement.
%
%   S3P is a 3D tensor containing all the S3P projection operators for all
%       frequencies. These operators can be applied to a different
%       time-frequency data (e.g., they can be computed from an empty room
%       data and applied to a brain recording data set). Note: these need
%       to be applied to the lead field matrix to perform source estimation
%       in the frequency domain.
%
%   SSSPdim is a vector containing the final noise subspace dimension for 
%       each frequency (i.e., the dimension selected with the percentile
%       filtered process).
%
%   f is a vector of frequencies corresponding t the S3P operators.
%
%   spec is a structure with the mean spectra for gradiometers and
%       magnetometers after pf-S3P denoising.
%
%   proj is a structure with all frequency specific projection vectors,
%       which can be used to increase the noise dimension at frequencies in
%       an offline mode.
%
%   goodtimeframes is a vector of indices of good time frames.   
%
%   mp_gr is the median power for gradiometers.
%
%   mp_ma is the median power for magnetometers.

if nargin<2
    error('Minimum number of input arguments is data and sampling rate')
end
if nargin<3 || isempty(freqs3p)
    freqs3p=[0 100]; % This specifies the frequency band at which to obtain S3P operators that apply baseline noise subspace dimension of s3pdim.
end
if nargin<4 || isempty(s3pdim)
    s3pdim=4;
end
if nargin<5 || isempty(freqpf)
    freqpf=[1 2 3 4]*60; % This specifies which frequenciues at which to apply the pf-S3P algorithm.
end
if nargin<6 || isempty(freqthresh)
    freqthresh=.35;
end
if nargin<7 || isempty(stop)
    freqstop=[srate/4 srate/2]; % This specifies frequency band to stop, to remove high frequency noise. To not do any low pass filtering set freqstop=[].
end
if nargin<8 || isempty(winsec)
    winsec=4;
end
if nargin<9 || isempty(winstepsec)
    winstepsec=winsec/2;
end
if nargin<10 || isempty(wind)
    wind=[2 3];
end
if nargin<11 || isempty(padfactor)
    padfactor=1;
end
if nargin<12 
    goodtimeframes=[];
end
if nargin<13 || isempty(ind_grad)
    ind_grad=1:306; ind_grad(3:3:306)=[];
end
if nargin<14 || isempty(ind_mag)
    ind_mag=3:3:306;
end
if nargin<15 || isempty(percentile)
    percentile=50;
end
szdata=size(data);
datamean=mean(data,2);
data=data-repmat(datamean,[1 szdata(2)]);
[sdata,spectral]=nsa_stft(data,srate,winsec,winstepsec,wind,padfactor);
szsdata=size(sdata);
if length(szsdata)==4
    multitaper=1;
else
    multitaper=0;
end
f=spectral.freq;
if length(freqs3p)==2
    ifrhp=find(f<freqs3p(1));
    ifrlp=find(f>freqs3p(2));
    fmaxsspi=min(ifrlp)-1;
    fminsspi=max(ifrhp)+1;
    if isempty(fmaxsspi)
        fmaxsspi=length(f);
    end
    if isempty(fminsspi)
        fminsspi=1;
    end
else
    error('freqs3p has to be a two-element vector specifying the band to')
end
indf=fminsspi:fmaxsspi;
% Getting indices for stop frequencies.
if ~isempty(freqstop)
    [~,indminstop]=min(abs(f-freqsstop(1)));
    [~,indmaxstop]=min(abs(f-freqsstop(2)));
    indstop=[indminstop indmaxstop];
end
f=f(indf);
if schar(freqpf)&&strcmp(freqpf,'same')
    freqpf=f;
end
if multitaper
    tfmag=squeeze(mean(mean(abs(sdata(:,:,ind_grad,:)).^2,3),4));
    tfgrad=squeeze(mean(mean(abs(sdata(:,:,ind_mag,:)).^2,3),4));
else
    tfmag=squeeze(mean(abs(sdata(:,:,ind_grad)).^2,3));
    tfgrad=squeeze(mean(abs(sdata(:,:,ind_mag)).^2,3));
end
if isempty(goodtimeframes)
    %%%%%%%%%%%%%%%%% Diagnostics of Spectral Amplitudes %%%%%%%%%%%%%%%%%%%%%%
    rms_spectro_mag=squeeze(sqrt(mean(tfmag,2))); % rms of magnetometers.
    rms_spectro_grad=squeeze(sqrt(mean(tfgrad,2))); % rms of gradiometers.
    rms=sqrt((rms_spectro_mag/norm(rms_spectro_mag)).^2 + (rms_spectro_grad/norm(rms_spectro_grad)).^2); % a sort of combined rms.
    goodtimeframes=find(rms>median(rms)-1.5*iqr(rms) & rms<median(rms)+1.5*iqr(rms)); % getting crazy outlier time frames.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
spec.spectrum_mag=sqrt(squeeze(mean(tfmag(goodtimeframes,:),1)));
spec.spectrum_grad=sqrt(squeeze(mean(tfgrad(goodtimeframes,:),1)));
lef=length(f);
SSSPdim=zeros(1,lef);
S3P=complex(zeros(szdata(1),szdata(1),lef));
UGR=complex(zeros(length(ind_grad),length(ind_grad),lef));
UMA=complex(zeros(length(ind_mag),length(ind_mag),lef));
SGR=zeros(length(ind_grad),lef);
SMA=zeros(length(ind_mag),lef);
sSgr=zeros(1,lef);
sSma=zeros(1,lef);
for fr=1:lef 
    if rem(fr,20)==0 || fr==1
        display(['processing S3P operator for frequency ' num2str(f(fr))])
    end
    if multitaper
        G=complex(zeros(szdata(1),szdata(1)));
        for tap=1:szsdata(4)
            sdataf=(squeeze(sdata(goodtimeframes,indf(fr),:,tap)))';
            G=G+(sdataf*sdataf');
        end
        G=G./(size(sdataf,2)*szsdata(4));
    else
        sdataf=(squeeze(sdata(goodtimeframes,indf(fr),:)))';
        G=(sdataf*sdataf')./(size(sdataf,2));
    end
    [Ugr,Sgr]=svd(G(ind_grad,ind_grad));
    [Uma,Sma]=svd(G(ind_mag,ind_mag));
    Sgr=diag(Sgr); 
    Sma=diag(Sma);
    UGR(:,:,fr)=Ugr;
    UMA(:,:,fr)=Uma;
    SGR(:,fr)=Sgr;
    SMA(:,fr)=Sma;
    sSgr(fr)=sum(Sgr(s3pdim+1:end));
    sSma(fr)=sum(Sma(s3pdim+1:end));
end
mp_gr=sSgr;
mp_ma=sSma;
%%%%% Percentile filtering %%%%%%
for k=1:length(freqpf) 
    [~,indff]=min(abs(freqpf(k)-f));
    ff=4;
    mp_gr(indff)=prctile(sSgr(max([1 indff-ff]):min([indff+ff lef])),percentile); 
    mp_ma(indff)=prctile(sSma(max([1 indff-ff]):min([indff+ff lef])),percentile);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
frkk=0;
proj=[];
for fr=1:lef 
    Ugr=UGR(:,:,fr);
    Uma=UMA(:,:,fr);
    Sgr=SGR(:,fr);
    Sma=SMA(:,fr);
    kk=0;
    s3pdim_gr=s3pdim; 
    while sSgr(fr)>mp_gr(fr)
        kk=kk+1; 
        sSgr(fr)=sum(Sgr(s3pdim+kk+1:end));
        s3pdim_gr=s3pdim+kk;
    end
    kk=0;
    s3pdim_ma=s3pdim;
    while sSma(fr)>mp_ma(fr)
        kk=kk+1;       
        sSma(fr)=sum(Sma(s3pdim+kk+1:end));
        s3pdim_ma=s3pdim+kk;
    end
    Uall=zeros(szdata(1),s3pdim_gr+s3pdim_ma);
    Uall(ind_grad,1:s3pdim_gr)=Ugr(:,1:s3pdim_gr);
    Uall(ind_mag,s3pdim_gr+1:end)=Uma(:,1:s3pdim_ma);
    [Uall,Sall]=svd(Uall,'econ');
    Sall=diag(Sall);
    s3pdim_all=s3pdim_gr+s3pdim_ma;
    for kk = 1:s3pdim_all  % Loop equivalent to mne_make_projector.m
        if Sall(kk)/Sall(1) < 1e-2
            s3pdim_all = kk;
            break;
        end
    end
    Uall=Uall(:,1:s3pdim_all);
    if any(abs(f(fr)-freqpf)<freqthresh)
        frkk=frkk+1;
        proj.grad{frkk}=Ugr(:,1:s3pdim_gr);
        proj.mag{frkk}=Uma(:,1:s3pdim_ma);
        proj.f(frkk)=f(fr);
    end
    sspf=eye(szdata(1))-Uall(:,1:s3pdim_all)*Uall(:,1:s3pdim_all)';
    S3P(:,:,fr)=sspf;
    SSSPdim(fr)=s3pdim_all;    
    if multitaper
        for tap=1:szsdata(4)
            sdata(:,indf(fr),:,tap)=(sspf*(squeeze(sdata(:,indf(fr),:,tap))'))';
        end
    else       
        sdata(:,indf(fr),:)=(sspf*squeeze(sdata(:,indf(fr),:))')';
    end
end
if multitaper
    tfmag=squeeze(mean(mean(abs(sdata(:,indf,ind_grad,:)).^2,3),4));
    tfgrad=squeeze(mean(mean(abs(sdata(:,indf,ind_mag,:)).^2,3),4));
else
    tfmag=squeeze(mean(abs(sdata(:,indf,ind_grad)).^2,3));
    tfgrad=squeeze(mean(abs(sdata(:,indf,ind_mag)).^2,3));
end
spec.spectrum_mag_s3p=sqrt(squeeze(mean(tfmag(goodtimeframes,:),1)));
spec.spectrum_grad_s3p=sqrt(squeeze(mean(tfgrad(goodtimeframes,:),1)));
spec.f=f;
if ~isempty(freqstop)
    if multitaper
        sdata(:,indstop,:,:)=0;
    else
        sdata(:,indstop,:)=0;
    end
end
[data_s3p]=nsa_istft(sdata,spectral);