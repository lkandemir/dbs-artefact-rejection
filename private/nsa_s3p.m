function [data_s3p,S3P,spectral,proj,SSP]=nsa_s3p(infile,outfile,options)

% This function performs all variants of the S^3P denoising algorithm. 
% It computes the S^3P projectors from the data in the infile fif file, 
% S^3P transforms this data, and saves it in the outfile fif file. 
% The S^3P projectors in the 3D array S3P or the frequency-specific 
% projection vectors in the structure proj can be used to S3P transform any
% other data set using the function nsa_s3p_transform.m. For example,
% nsa_s3p.m can be run on a room recording containing artifacts but no 
% subject, and then applied to a brain recording using nsa_s3p_transform.m.
% The behavior of nsa_s3p.m is controlled by the structure options, which 
% can be used to perform all S^3P variants, including a reference noise 
% channel algorithm, and all versions can be performed using multitaper 
% spectral analysis in addition to single taper analysis.
%
% usage: [data_s3p,S3P,spectral,proj,SSP]=nsa_s3p(infile,outfile,options)
%
% inputs: 
%
%   infile is the name of the fif file to process.
%
%   outfile is the name of the fif file to output.
%
% optional inputs:
%
%   options is a structure with many important parameters that control the
%       behavior of this program. If not included or empty, default 
%       parameters are used.
%
%   The fields of the options structure are:
%
%   options.freqs3p is a two element vector that defines the frequency
%       range to perform S^3P using a baseline noise subspace dimension.
%       (default: options.freqs3p=[0 125];)
%
%   options.s3pdim is the S^3P baseline noise dimension for each frequency, 
%       which can be zero, if one desires only to remove outlier spectral 
%       peaks via frequency specific spatial transformations. 
%       (default: options.s3pdim=1) 
%
%   options.freqpf is a vector of selective frequencies where to apply the 
%       pf-S^3P algorithm. 
%       (default: options.freqpf=[]; % don't do the pf-s3p process)
%
%       Other important functional options and examples:
%           options.freqpf='same'; % do pf-S^3P at the same frequencies as 
%               S^3P, using a sliding window (i.e., sliding window across 
%               frequencies in the range defined by options_def.freqs3p)
%               Keep in mind that this approach can remove brain spectral 
%               peaks if pf-S^3P is performed on brain data with peaks in 
%               the frequency range where pf-S^3P is applied to.
%
%           options.freqpf=[1:3]*60; % suppress USA power noise line frequencies and harmonics up to 180 Hz.
%           options.freqpf=[1:4]*50; % suppress Europe power noise line frequencies and harmonics up to 200 Hz.
%
%   options.freqthresh is a threshold to allow matches between provided noise
%       frequencies (in vector freqpf) and those obtained from the FFT.
%       This was added to deal with users that sampled at strange sampling
%       rates. (e.g., if freqpf=60; but the closest frequencies were
%       within freqpf +/- freqthresh, these neighbor frequencies will be
%       included in the actual freqpf vector to be used).
%       (default: freqthresh=.25)
%
%   options.fmax is the maximum frequency of interest in Hz, which
%       specifies the corner frequency for optional direct IFFT-based 
%       "low-pass filtering" (setting Fourier coefficients to zero before 
%       inverse Fourier transform).
%       (default: options.fmax=options.freqs3p(2)=125;)
%
%   options.refind is either: 1) an index or a vector of indices of channels; 
%       or 2) string specifying the name of a channel (e.g.,  options.refind='MEG1533');
%       or 3) a cell array with names of channels. The channel(s) selected is used as a noise 
%       reference. This performs yet another extension of S^3P (not 
%       discussed in the NeuroImage 2011 paper due to space limitations) in 
%       which the noise subspace is constructed using weights that depend on
%       reference signals. If you don't want to use the reference algorithm,
%       leave it empty, options.refind=[].
%       (default: options.refind=[];)
%
%   options.winsec is the window duration in seconds for time-frequency analysis.
%       (default: options.winsec=4;)
%
%   options.winstepsec is the duration of the time step in seconds for the
%       spectrogram. You will get a time frame every options.winstepsec seconds.
%       (default: options.winstepsec=options.winsec/4;)
%
%   options.wind can be several things: (default: options.wind=[2 3])
%           For single taper analysis:
%               wind can be string of a window (e.g., wind='kaiser';
%                wind='hanning'; wind='gausswin'; wind='hamming'; etc.).
%               or 
%               wind can be a numeric vector of a user-defined taper with
%               length equal to the nfft length corresponding to winsec.
%               (e.g., wind=kaiser(2000));
%           For multitaper spectral analysis, wind=[NW K]; a two-element vector, where      
%               NW = time bandwidth parameter (e.g. NW=2;)
%               K = number of data tapers kept, usually 2*NW-1 (e.g. 3, 5 or 7)
%                   For multitaper option: wind=[NW K]=[2 3]; is
%                   recommended.
%               or
%               wind can be a matrix (nfft x tapers) to use user-defined
%               multitaper/multiwavelets windowing functions.
%
%   options.padfactor is the zero padding factor.
%       (default: options.padfactor=1; meaning no zero padding.)
%
%   options.goodtimeframes is a vector of time frames to include in the creation
%       of the S^3P projectors. This should be used only if one needs to 
%       specify selective time frames (e.g., to match a different analysis)
%       or if you want to override the outlier detection process built into the
%       function and instead use all time-frames regardless of outlier time-frames
%       with very large or very low power.
%       Note: if this input is not included or if it's empty, the function
%       will actually automatically detect outliers as time frames with
%       total power larger or smaller that the median power +/- 1.5*iqr.
%       This is done to avoid making projectors from extremely noisy
%       non-stationary time frames or time frames with odd low power (e.g.,
%       time frames in which sensors went flat).
%       (default: options.goodtimeframes=[];)
%
%   options.ind_grad is a vector of indices of good gradiometers.
%       (default: options.ind_grad=1:306; options.ind_grad(3:3:306)=[]; meaning all gradiometers)
%
%   options.ind_mag is a vector of indices of good magnetometers.
%       (default: options.ind_mag=3:3:306; meaning all magnetometers)
%
%   options.percentile is the percentile to use for the percentile filtering process
%       used to remove outlier peaks with pf-S^3P.
%       (default: options.percentile=50; % that is the median)
%
%   options.DCoffset is a flag to remove the overall mean of each channel before
%       processing and after all the S^3P and inverse transformations.
%       (default: options.DCoffset=1; % meaning do remove mean)
%
%   For example, options = 
%        winsec: 4
%    winstepsec: 1
%        freqpf: [60 120]
%        s3pdim: 1
%        refind: 'EEG061'        
%        winsec: 4
%       freqs3p: [0 125]
%   will run S3P with a baseline noise subpace dimension of 1, using the
%   EKG channel named 'EEG061' as a reference for denoising within the
%   frequency band from 0 to 125Hz (the default), and apply the percentile 
%   filtered S3P additional process only at the American power line noise 
%   frequency and first harmonic. Other unspecified parameters will take 
%   default values.
%
% outputs: 
%
%   data_s3p is the data time series after S3P projections and inverse
%       time-frequency trasnformations have been applied.
%
%   S3P is a 3D tensor containing all the S3P projection operators for all
%       frequencies. These operators can be applied to a different
%       time-frequency data (e.g., they can be computed from an empty room
%       data and applied to a brain recording data set).
%
%   spectral is a spectral data structure with important information about 
%       the time-frequency transfomation, S3P, including a record of the 
%       input parameters. This stucture is needed by nsa_s3p_transform.m      
%
%   proj is a structure with all the frequency specific projection vectors,
%       for both gradiometers and magnetometers, corresponding to the 
%       frequencies and noise dimensions, in the spectral structure. 
%       This stucture is needed by nsa_s3p_transform.m 
%
%   SSP is a structure with important information about the original rank
%       of the magnetometer and gradiometers time series, and the rank of
%       these two time series after the S^3P transformations. If the rank is
%       not reduced by S^3P, then essentially, there is no effective
%       time-domain equivalent SSP operator. However, if the S^3P
%       transformations reduced the rank of the two time series, then the
%       program automatically determines what would be the equivalent
%       projection vectors for magnetometers and gradiometers, and the
%       effective time-domain SSP projector. Note that S3P 'magically' can
%       remove noise patterns (e.g., EKG) and yet not reduced the rank of the
%       multivariate time series. Also, because it transforms the
%       gradiometers and magnetometers separately, it might appear to
%       actually increase the rank of the joint time series (if this one 
%       was processed with SSS). Regular SSP does this too! This is
%       related to how SSS combines magnetometers and gradiometers using
%       an arbitray scaling factor, before computing the multipolar moments.
%       However, time domain SSP indeed reduces the rank of the separate 
%       gradiometer and magnetometer time series. S3P does not necessarily
%       reduce the rank at all, unless the noise spatial patterns are fixed
%       across frequencies, in which case, the frequency domain SSP (FD-SSP) 
%       algorithm would have been sufficient for denoising.
%       This behavior of S3P might appear miraculous for the unitiated, 
%       but it makes perfect sense and is exactly indeed what is expected, 
%       and has very positive implications about the possible lack
%       of a need to project the leadfield matrix before source imaging in
%       the time-domain for most circumstances. If the 
%       SSP.effective_projector is not empty, then it should be applied to 
%       the lead field matrix for source imaging. 
%       For questions and/or comments, e-mail me, as these details where just 
%       briefly alluded to in the paper. -Rey
%
% Reference:
%
% Spectral signal space projection algorithm for frequency domain MEG and
% EEG denoising, whitening, and source imaging.
% Ram�rez RR, Kopell BH, Butson CR, Hiner BC, Baillet S.
% Neuroimage. 2011 May 1;56(1):78-92. Epub 2011 Feb 19.  
%
% e-mail: rrramir@uw.edu

% Copyright (C) 2011- Rey Rene Ramirez
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 2 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program; if not, write to the Free Software
% Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

if nargin<2
    error('Minimum number of input arguments is two: input and output datafile names.')
end
%%%%%%%%%%%%%% Define Defaults Options for easy editing %%%%%%%%%%%%%%%%%%%
options_def.freqs3p=[0 125]; % This specifies the frequency band at which to obtain S3P operators that apply baseline noise subspace dimension of s3pdim.
options_def.s3pdim=1;
options_def.freqpf=(1:2)*60;
options_def.freqthresh=.25;
options_def.fmax=options_def.freqs3p(2);
options_def.refind=[];
options_def.winsec=4;
options_def.winstepsec=options_def.winsec/4;
options_def.wind=[2 3];
options_def.padfactor=1;
options_def.goodtimeframes=[];
options_def.ind_grad=1:306; options_def.ind_grad(3:3:306)=[];
options_def.ind_mag=3:3:306;
options_def.percentile=50;
options_def.DCoffset=1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin<3 || isempty(options)
    options=options_def;
else
    % Now adding missing fields if options structure was inputted.
    if ~isfield(options,'freqs3p')
        options.freqs3p= options_def.freqs3p; 
    end
    if ~isfield(options,'fmax')
        options.fmax=options_def.fmax;
    end
    if ~isfield(options,'s3pdim')
        options.s3pdim= options_def.s3pdim;
    end
    if ~isfield(options,'freqpf')
        options.freqpf= options_def.freqpf;
    end
    if ~isfield(options,'freqthresh')
        options.freqthresh= options_def.freqthresh;
    end
    if ~isfield(options,'refind')
        options.refind=options_def.refind;
    end   
    options_def.refind=[];
    if ~isfield(options,'winsec')
        options.winsec=options_def.winsec;
    end
    if ~isfield(options,'winstepsec')
        options.winstepsec=options_def.winstepsec;
    end
    if ~isfield(options,'wind')
        options.wind=options_def.wind;
    end
    if ~isfield(options,'padfactor')
        options.padfactor=options_def.padfactor;
    end
    if ~isfield(options,'goodtimeframes')
        options.goodtimeframes=options_def.goodtimeframes;
    end
    if ~isfield(options,'ind_grad')
        options.ind_grad=options_def.ind_grad;
    end
    if ~isfield(options,'ind_mag')
        options.ind_mag=options_def.ind_mag;
    end
    if ~isfield(options,'percentile')
        options.percentile=options_def.percentile;
    end
    if ~isfield(options,'DCoffset')
        options.DCoffset=options_def.DCoffset;
    end 
end
if ~isempty(options.refind) && (options.s3pdim==0 || isempty(options.freqs3p))
    error('If using the reference S3P algorithm, the baseline noise dimension (options.s3pdim) should not be zero, and the s3p frequency range (options.freqs3p) should not be empty.')
end
ind_all=unique([options.ind_grad options.ind_mag]);
[data,fiffsetup,~,~,~]=nsa_read_raw_elekta(infile);
szdata=size(data);
if options.DCoffset % Removing mean outside of nsa_spectralssp.m to avoid duplicating data.
    display('Removing overall mean of each channel')
    datamean=mean(data,2);
    data=data-repmat(datamean,[1 szdata(2)]);
end
%%%%%%%%% Dealing with options for Noise Reference S^3P algorithm %%%%%%%%%
if isnumeric(options.refind) 
    options.dataref=data(options.refind,:);
elseif iscell(options.refind) 
    ind=[]; for k=1:length(options.refind); ind=[ind find(strcmp(fiffsetup.info.ch_names,options.refind{k}))]; end %#ok<AGROW>
    options.dataref=data(ind,:);
elseif ischar(options.refind)
    ind=strcmp(fiffsetup.info.ch_names,options.refind);
    options.dataref=data(ind,:);
else
    options.dataref=[];
end    
data=data(ind_all,:);
srate=fiffsetup.info.sfreq; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%% S^3P algorithms %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[data_s3p,S3P,spectral,proj,SSP]=nsa_spectralssp(data,srate,options);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if options.DCoffset 
    display('Removing mean of each channel')
    data_s3pmean=mean(data_s3p,2);
    data_s3p=data_s3p-repmat(data_s3pmean,[1 size(data_s3p,2)]);
    spectral.DCoffset=1;
end
display('Now writing new fif file')
nsa_mne_ex_read_write_raw(infile,outfile,data_s3p);