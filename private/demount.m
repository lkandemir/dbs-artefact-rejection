function [bp, channels] = demount(channels, flag, type)
    %% Function to separate magnetometers and gradiometers. It can also be used to eliminate bad channels. 
    % Inputs:
    % --- channels (cell-array): channel names
    % --- flag (double-array): indicating bad channels
    % --- type (char-array): 'megmag' for magnetometers or 'megplanar' for % gradiometers.
    % Output:
    % --- bp (double): Blueprint file for the sensor placement. 
    type=lower(type);
    flag(flag<1)=0;
    switch type
        case 'neuromag'
            bp=and(contains(channels, 'MEG'), flag);
        case 'ctf'
            bp=and(strcmp(channels, 'MEG'), flag);
        case 'megmag'
            bp=and(strcmp(channels, 'MEG MAG'), flag);
        case 'megplanar'
            bp=and(strcmp(channels, 'MEG GRAD'), flag);
    end
    
end