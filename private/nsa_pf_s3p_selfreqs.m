function [data_s3p,S3P,SSSPdim,f,spec,proj,goodtimeframes]=nsa_pf_s3p_selfreqs(infile,outfile,freqs,freqthresh,s3pdim,winsec,winstepsec,wind,padfactor,goodtimeframes,ind_grad,ind_mag)

% usage: [data_s3p,S3P,SSSPdim,f,spec,proj,goodtimeframes,mp_gr,mp_ma]=nsa_pf_s3p_selfreqs(infile,outfile,s3pdim,goodtimeframes,s3pdimfixed,padfactor,freqband,winsec,ind_grad,ind_mag,joint,freqs)
%
% inputs: 
%
%   infile is the name of the fif file to process.
%
%   outfile is the name of the fif file to output.
%       Note: this time series has been spatially transformed in the
%       frequency doman, and hence should not be localized like a standard
%       time series.
%
%   freqs this is a vector of selective frequencies where to apply the 
%       pf-S^3P algorithm. Note how this version of the algorithm is different
%       from the more basic pf-S^3P algorithm that applies the percentile 
%       filtered process to all frequencies.
%       (default: freqs=60*[1 2 3 4]);
%
%   freqthresh is a threshold to allow matches between provide noise
%       frequencies (in vector freqs) and those obtained from the FFT.
%       This was added to deal with users that sampled at weird sampling
%       rates. (e.g., if freqs=60; but the closest frequencies were
%       within freqs +/- freqthresh, these neighbor frequencies will be
%       included in the actual freqs vector to be used).
%       (default: freqthresh=.35)
%
%   s3pdim is the s3p baseline noise dimension for each frequency, 
%       which can be zero, if one desires only to remove outlier spectral 
%       peaks. (default: s3pdim=4)
%
%   winsec is the window duration in seconds for time-frequency analysis.
%       (default: winsec=4;)
%
%   padfactor is the zero padding factor.
%       (default: padfactor=1; meaning no zero padding.)
%
%   goodtimeframes is a vector of time frames to include in the creation
%       of the S3P projectors. This should be used only if one needs to 
%       specify selective time frames (e.g., to match a different analysis).
%       Note: if this input is not included or if it's empty, the function
%       will actually automatically detect outliers as time frames with
%       total power larger or smaller that the median power +/- 1.5*iqr.
%       This is done to avoid making projectors from extremely noisy
%       non-stationary time frames or time frames with odd low power.
%       (default: goodtimeframes=[];)
%
%   ind_grad is a vector of indices to good gradiometers.
%       (default: ind_grad=1:306; ind_grad(3:3:306)=[]; meaning all gradiometers)
%
%   ind_mag is a vector of indices to good magnetometers.
%       (default: ind_mag=3:3:306; meaning all magnetometers)
%
% outputs: 
%
%   data_s3p is the data time series after S3P projections and inverse
%       time-frequency trasnformation. Note: This data should not be
%       localized like any regular time-series as spatial projections have
%       been applied to specific frequencies, and hence it has to be
%       localized in the frequency domain. It is important you understand
%       the meaning of this statement.
%
%   S3P is a 3D tensor containing all the S3P projection operators for all
%       frequencies. These operators can be applied to a different
%       time-frequency data (e.g., they can be computed from an empty room
%       data and applied to a brain recording data set). Note: these need
%       to be applied to the lead field matrix to perform source estimation
%       in the frequency domain.
%
%   SSSPdim is a vector containing the final noise subspace dimension for 
%       each frequency (i.e., the dimension selected with the percentile
%       filtered process).
%
%   f is a vector of frequencies corresponding t the S3P operators.
%
%   spec is a structure with the mean spectra for gradiometers and
%       magnetometers after pf-S3P denoising.
%
%   proj is a structure with all frequency specific projection vectors,
%       which can be used to increase the noise dimension at frequencies in
%       an offline mode.
%
%   goodtimeframes is a vector of indices of good time frames.    

if nargin<2
    error('Minimum number of input arguments is two: input and output datafile names.')
end
if nargin<3 || isempty(freqs)
    freqs=[1 2 3 4]*60;
end
if nargin<4 || isempty(freqthresh)
    freqthresh=.35;
end
if nargin<5 || isempty(s3pdim)
    s3pdim=4;
end
if nargin<6 || isempty(winsec)
    winsec=4;
end
if nargin<7 || isempty(winstepsec)
    winstepsec=winsec/2;
end
if nargin<8 || isempty(wind)
    wind=[2 3];
end
if nargin<9 || isempty(padfactor)
    padfactor=1;
end
if nargin<10 || isempty(goodtimeframes)
    goodtimeframes=[];
end
if nargin<11 || isempty(ind_grad)
    ind_grad=1:306; ind_grad(3:3:306)=[];
end
if nargin<12 || isempty(ind_mag)
    ind_mag=3:3:306;
end
ind_all=unique([ind_grad ind_mag]);
[data,fiffsetup,~,~,~]=nsi_read_raw_elekta(infile);
data=data(ind_all,:);
srate=fiffsetup.info.sfreq;
[data_s3p,S3P,SSSPdim,f,spec,proj,goodtimeframes]=nsa_pf_spectralssp_selfreqs(data,srate,freqs,freqthresh,s3pdim,winsec,winstepsec,wind,padfactor,goodtimeframes,ind_grad,ind_mag);
display('Now writing new fif file')
nsi_mne_ex_read_write_raw(infile,outfile,data_s3p);