function varargout = process_ICA_MI1( varargin )
% PROCESS_ICA_MI1: ICA decomposition. 
%
% =============================================================================@
% Authors: Levent Kandemir, 2018
% 
% Code inspired from:
% O. Abbasi, J. Hirschmann, G. Schmitz, A. Schnitzler, M. Butz. Rejecting 
%  deep brain stimulation artefacts from MEG data using ICA and mutual
%  information. J. Neurosci. Methods, 268 (2016), pp. 131-141.

eval(macro_method);
end


%% ===== GET DESCRIPTION =====
function sProcess = GetDescription() %#ok<DEFNU>
    % === Description the process
    sProcess.Comment     = 'DBS Artifact Removal - ICA Calculation';
    sProcess.FileTag     = 'IcaComponents';
    sProcess.Category    = 'Filter';
    sProcess.SubGroup    = 'DBS_Noise';
    sProcess.Index       = 66;
    
    % === Definition of the input accepted by this process
    sProcess.InputTypes  = {'data', 'results', 'raw', 'matrix'};
    sProcess.OutputTypes = {'data', 'results', 'raw', 'matrix'};
    sProcess.nInputs     = 1;
    sProcess.nMinFiles   = 1;
    
    sProcess.options.meg.Comment    = 'Gradiometers Only:  <FONT color="#777777">Applies for Elekta Neuromag recordings.';
    sProcess.options.meg.Type       = 'checkbox';
    sProcess.options.meg.Value      = 0;
    
    % ==== ICA Algorithm
    sProcess.options.label1.Comment = '<B>ICA Options</B>';
    sProcess.options.label1.Type    = 'label';
    sProcess.options.tica.Comment = {'Runica Algorithm', 'Fastica Algorithm'; 'rica', 'fica'};
    sProcess.options.tica.Type    = 'radio_label';
    sProcess.options.tica.Value   = 'rica'; 
    sProcess.options.ncomp.Comment = 'Percentage of Components: <FONT color="#777777">0 for equal number of channels.';
    sProcess.options.ncomp.Type    = 'value';
    sProcess.options.ncomp.Value   = {0,'',0};
    
    % === Epoch length will be used to divide the data. 
    sProcess.options.label3.Comment = '<B>Other Options</B>';
    sProcess.options.label3.Type    = 'label';
    sProcess.options.epoch.Comment = 'Epoch Length: ';
    sProcess.options.epoch.Type    = 'value';
    sProcess.options.epoch.Value   = {10,'Sec. ',2};
end


%% ===== FORMAT COMMENT =====
function Comment = FormatComment(sProcess) % Giving information to the user. 
    if sProcess.options.meg.Value
        Comment = 'Only Gradiometers Will Be Processed!';
    else
        Comment = 'Gradiometers and Magnetometers Will Be Processed Separately!';
    end   
end


%% ===== RUN =====
function sInput = Run(sProcess,sInput) % Main function.

    %% Required Paths
    target=strcat(getfield(bst_get('ProtocolInfo'), 'STUDIES'),'\',sInput.SubjectName,...
        '\',getfield(bst_get('Study'), 'Name'),'\','ICAcomponents.mat');
    
    %% Load Primary Files
    channelfile=load(file_fullpath(sInput.ChannelFile));
    temp=load(file_fullpath(sInput.FileName));

    %% Load recording details and prepare function return
    orj=sInput.A; %#ok<NASGU>
    
    %% Extract settings
    fsamp=temp.F.prop.sfreq;
    epoch=sProcess.options.epoch.Value{1};
    tica=sProcess.options.tica.Value;
    ncomp=sProcess.options.ncomp.Value{1};
    
    %% Reject bad segments
    map=badseg(temp.F.events, sInput.TimeVector); 
    ndat=sInput.A(:,logical(map));

    %% Calculations depending on type of sensors 
    if strcmp(lower(temp.Device), 'neuromag')
        nmag=1;
        bp = demount({channelfile.Channel(:).Type}', sInput.ChannelFlag, 'megplanar'); 
    else
        nmag=0;
        bp = demount({channelfile.Channel(:).Type}', sInput.ChannelFlag, temp.Device); 
    end
    
    % Reduce sensors to gradiometers.
    mat=ndat(bp,:);
    numcomp=floor(sum(bp)*ncomp/100);
    
    % Do ICA and save results. 
    [comp, unmixing] = calcICA(mat, fsamp, epoch, tica, numcomp);                            
    save(target, 'comp', 'unmixing', 'bp', 'orj', 'mat', 'map', 'epoch','-v7.3');
    
    % Unless stated otherwise, do ICA also on magnetometers in Elekta
    % Neuromag recordings. 
    if ~sProcess.options.meg.Value && nmag==1
        bp1 = demount({channelfile.Channel(:).Type}',sInput.ChannelFlag,'megmag');            
        mat1=ndat(bp1,:);
        numcomp=floor(sum(bp1)*ncomp/100);
        [comp1, unmixing1] = calcICA(mat1, fsamp, epoch, tica, numcomp);                            
        save(target, 'comp1', 'unmixing1', 'bp1', 'mat1', '-append');
    end       

end


%% ICA Calculation
function [comp, unmixing] = calcICA(dat, fsamp, epoch, tica, ncomp)

    %% Function to calculate ICA. 
    % [comp, unmixing] = calcICA(dat, fsamp, epoch, tica, ncomp)
    % For code-based usage: [mat, bp] = process_ICA_MI1('calcICA', dat, fsamp, epoch, tica, ncomp); 
    % Where;
    % --- dat (double): Continuous matrix with Channels x Time-Series.
    % --- fsamp (double): Sampling frequency of the data. 
    % --- epoch (double): Length of each trial in seconds. 
    % --- tica (character array): type of ICA algorithm. 'runica' or 'fastica'.
    % --- ncomp (double) number of components to be calculated in ICA.
    % Output:
    % --- comp (cell-array): Independent components as trials.  
    % --- unmixing (double): Unmixing matrix. 
                                                                                                                              
    % Divide data to epochs without loss.                               
    trial=divide(dat, fsamp, epoch);
    nTrials=size(trial,2);
    
    % Baseline correction. It is safer to remove means for each trial rather than all data. 
    for i=1:nTrials
        temp=mean(trial{i},2);
        trial{i}=trial{i}-temp;
    end
    
    % Scaling for fast ICA calculation. Scaling reduces ICA calculation time to great extent. 
    temp                 = trial{1};
    temp(~isfinite(temp)) = 0; 
    scale = norm((temp*temp')./size(temp,2)); clear temp;
    scale = sqrt(scale);
    if scale ~= 0
        for i=1:nTrials
            trial{i} = trial{i} ./ scale;
        end
    end
    
    % Concatenate trials. This will only be used to calculate unmixing matrix. 
    % Runica prefers continuous data. Component calculation will be carried out with trials.   
    for i=1:nTrials
        beg=((i-1)*fsamp*epoch)+1;
        en=((i-1)*fsamp*epoch)+size(trial{i},2);
        dat(:,beg:en)=trial{i};
    end
    
    switch tica
        % Runica
        case 'rica'
            if ncomp
                opt=[{'lrate'}, {0.001},{'reset_randomseed'}, {0}, {'pca'}, {ncomp}];
            else
                opt=[{'lrate'}, {0.001},{'reset_randomseed'}, {0}];  % Settings derived from Fieldtrip's default settings
            end
            [weights, sphere] = runica(dat, opt{:});
            sphere = sphere./norm(sphere);
            unmixing = weights*sphere;
     
            comp=cell(nTrials,1);
            for i=1:nTrials
                comp{i} = scale * unmixing * trial{i};
            end
        % Fastica
        case 'fica'
            if ncomp
                [~, unmixing] = fastica (dat, 'numOfIC', ncomp);
            else
                [~, unmixing] = fastica (dat);
            end
            
            comp=cell(nTrials,1);
            for i=1:nTrials
                [mixedsig, mixedmean] = remmean(trial{i});
                [~, NumOfSampl] = size(mixedsig);
                comp{i} = unmixing * mixedsig + (unmixing * mixedmean) * ones(1, NumOfSampl);
            end 
    end
end


%% Divide to Epochs
function trial=divide(dat, fsamp, epoch)

    %% Function to divide continuous data to trials. 
    % Use as trial=divide(dat, fsamp, epoch)
    % For code-based usage: trial= process_ICA_MI1('divide', dat, fsamp, epoch); 
    % Where;
    % --- dat (double): Continuous data.
    % --- fsamp (double): Sampling frequency of the data. 
    % --- epoch (double): Unmixing matrix calculated by ICA.
    % Output:
    % --- trial (cell-array): Divided data to trials.
    
    [~,nS]=size(dat);
    eLen=fsamp*epoch; % epoch length  
    if mod(nS,eLen)>0
        nTrials=floor(nS/eLen);
        trial=cell(1,nTrials+1);
    else
        nTrials=nS/eLen;
        trial=cell(1,nTrials);
    end
    
    %divide data to epochs without loss
    for i=1:nTrials
        beg=((i-1)*eLen)+1;
        e=i*eLen;
        trial{i}=dat(:,beg:e);
    end
    if mod(nS,eLen)>0
        ind=nTrials+1;
        beg=((ind-1)*eLen)+1;
        trial{nTrials+1}=dat(:,beg:nS);
    end
end

